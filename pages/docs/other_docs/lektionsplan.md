---
title: '24F PBa it sikkerhed'
subtitle: 'Fagplan for netværks- og kommunikationssikkerhed'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Fagplan for netværks- og kommunikationssikkerhed
skip-toc: false
semester: 24F
hide:
  - footer
---

# Lektionsplan

| Uge | Underviser | Indhold                                                                                                       |
| --- | ---------- | ------------------------------------------------------------------------------------------------------------- |
| 06  | NISI       | Introduktion til faget - overblik og værktøjer.                                                               |
| 07  | NISI       | OSI modellen, netværksenheder (routere, switch, AP), protokoller (802.3, 802.11), sniffe trafik med wireshark |
| 08  | NISI       | Segmentering (interfaces, VLANs, netværk), firewalls, teste netværk med NMAP                                  |
| 09  | NISI       | Bygge og dokumentere netværk (IPAM, CIS18 - hærde opnsense ifølge benchmarks)                                 |
| 10  | NISI       | Bygge sikrede netværk: sniffe trafik, segmentering, netflow, filterlog, firewalls DNS                                                    |
| 11  | NISI       | Overvågning: logs, netværkstrafik, graylog (ISO kontroller og alarmering)                                     |
| 12  | NISI       | Kryptering af netværkstrafik (Certifikater, VPN, DNS)                                                         |
| 14  | NISI       | Trådløs sikkerhed (Certifikater, WEP, WPA2/3, 802.1x)                                                         |
| 15  | NISI       | Intrusion Detection og Prevention (Suricata IDS/IPS)                                                          |
| 16  | NISI       | Netværksangreb og MITRE (Wazuh og atomic red team)                                                            |
| 18  | NISI       | Purple teaming og rapportering                                                                                |
| 19  | NISI       | Projektuge 1                                                                                                  |
| 20  | NISI       | Projektuge 2                                                                                                  |
| 21  | NISI       | Projektuge 3                                                                                                  |

<!--

| Uge | Underviser | Indhold                                                                                                                       |
| --- | ---------- | ----------------------------------------------------------------------------------------------------------------------------- |
| 06  | NISI       | _Grundlæggende_ Introduktion til faget - overblik og værktøjer.                                                               |
| 07  | NISI       | _Grundlæggende_ OSI modellen, netværksenheder (routere, switch, AP), protokoller (802.3, 802.11), sniffe trafik med wireshark |
| 08  | NISI       | _Design og konstruktion_ Segmentering (interfaces, VLANs, netværk), firewalls, teste netværk med NMAP                         |
| 09  | NISI       | _Design og konstruktion_ Bygge netværk og dokumentere netværk (IPAM, CIS18)                                                   |
| 10  | NISI       | _Grundlæggende_ Netværks protokoller: Wireshark, sniffe trafik, HTTP, HTTPS, m. fl.                                           |
| 11  | NISI       | _Overvågning_ Monitorering: syslog, netflow, graylog (ISO kontroller og alarmering)                                           |
| 12  | NISI       | _Overvågning_ Kryptering af netværkstrafik (Certifikater, VPN, DNS)                                                           |
| 14  | NISI       | _Grundlæggende_ Trådløs sikkerhed (Certifikater, WEP, WPA2/3, 802.1x)                                                         |
| 15  | NISI       | _Overvågning_ Intrusion Detection og Prevention (Suricata IDS/IPS)                                                            |
| 16  | NISI       | _Test_ Netværksangreb og MITRE (Wazuh og atomic red team)                                                                     |
| 18  | NISI       | _Test_ Purple teaming og rapportering, oplæg til projektuger                                                                  |
| 19  | NISI       | Projektuge 1                                                                                                                  |
| 20  | NISI       | Projektuge 2                                                                                                                  |
| 21  | NISI       | Projektuge 3                                                                                                                  |

Grundlæggende
Design
Konstruktion
Test
Implementation

**_Den studerende har viden om og forståelse for_**

- [uge6,7] Hvilke enheder, der anvender hvilke protokoller (grundlæggende)
- [uge7] Adressering i de forskellige lag (grundlæggende)

- [uge8] Gængse netværksenheder der bruges ifm. sikkerhed (firewall, IDS/IPS, honeypot, DPI) (grundlæggende)
- [alle_uger] Netværkstrusler (grundlæggende)
- [uge14] Trådløs sikkerhed (grundlæggende)
- [uge10] Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl) (grundlæggende)
- [ ] Sikkerhed i TCP/IP (grundlæggende)

- [uge11] Netværk management (overvågning/logning, snmp) (design/overvågning)
- [uge11] Forskellige sniffing strategier og teknikker (overvågning)
- [uge12] Forskellige VPN setups (overvågning)

### Færdigheder

**_Den studerende kan_**

- [uge15] Overvåge netværk samt netværkskomponenter, (f.eks. IDS eller IPS, honeypot) (overvågning)
- [uge16] Teste netværk for angreb rettet mod de mest anvendte protokoller (overvågning)
- [uge15+16] Identificere sårbarheder som et netværk kan have. (design/overvågning)

### Kompetencer

**_Den studerende kan håndtere udviklingsorienterede situationer herunder_**

- [alle_uger] Designe, konstruere og implementere samt teste et sikkert netværk (grundlæggende/design/overvågning)
- [uge15] Opsætte og konfigurere et IDS eller IPS (design)
- [uge15+16] Monitorere og administrere et netværks komponenter (overvågning)
- [uge18] Udfærdige en rapport om de sårbarheder et netværk eventuelt skulle have (red team report) (overvågning) -->

# Studieaktivitets modellen

![study activity model](Study_Activity_Model.png)

## Andet

Intet på nuværende tidspunkt
