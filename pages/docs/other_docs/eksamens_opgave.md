---
title: '24F PBa it sikkerhed'
subtitle: 'Eksamen netværks- og kommunikationssikkerhed'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Eksamen netværks- og kommunikationssikkerhed
semester: 24F
---

# Dokumentets indhold

Dette dokument indeholder praktiske informationer samt emner til eksamen i faget *Netværks- og kommunikationssikkerhed* afholdt i forårssemestret 2024

# Eksamens beskrivelse

Eksamen er beskrevet i den [institutionelle del af studieordningen](https://esdhweb.ucl.dk/D24-2490519.pdf) _afsnit 5.2.4_

Tidsplan for eksamen kan findes på wiseflow.  
Eksamen er med ekstern bedømmelse og bedømmes efter 7-trinsskalaen.  

Tidligere dokumenter:  

- [Semesterplanen](https://esdhweb.ucl.dk/D23-2156677.pdf)
- [Fagets hjemmeside](https://ucl-pba-its.gitlab.io/24f-its-nwsec/)
- [Lektionsplan](../other_docs/lektionsplan.md)
- [Studieordningen mm.](https://www.ucl.dk/studiedokumenter/it-sikkerhed)

*Fra studieordningen*

Prøven er en individuel mundtlig prøve af en varighed på 25 minutter (inkl. votering) på baggrund af et
skriftligt produkt.  

Det skriftlige produkt udarbejdes enten individuelt eller i en gruppe med op til 4 studerende.  
Den skriftlige aflevering må maksimum være 15 normalsider foruden bilag uanset gruppestørrelse.  
En normalside er 2.400 tegn inkl. mellemrum og fodnoter.  
Forside, indholdsfortegnelse, litteraturliste samt bilag tæller ikke med heri.   
Bilag er uden for bedømmelse og kan ikke forventes læst.  

Prøven bedømmes efter 7-trinsskalaen med ekstern bedømmelse.

I henhold til Eksamensbekendtgørelsen foretages der altid en individuel bedømmelse.  
Bedømmelsen er en helhedsvurdering af den skriftlige og mundtlige præstation.  
Der er ikke krav til individualisering af det skriftlige gruppearbejde.

# Eksamens datoer

Se [semesterbeskrivelsen](https://esdhweb.ucl.dk/D23-2442002.pdf)


