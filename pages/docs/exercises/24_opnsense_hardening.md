---
hide:
  - footer
---

# Øvelse 24 - opnsense hærdning

## Information

Denne øvelse skal laves som gruppe. 

CIS (Center for Internet Security) Critical Security Controls, også kendt som CIS Controls, er en omfattende liste over anbefalinger og bedste praksis til at forbedre sikkerheden i informationssystemer.  
CIS Controls er designet til at hjælpe organisationer med at beskytte sig mod de mest almindelige trusler og angreb.  
CIS18 er nyeste udgave af kontrollerne og kan findes hos [cisecurity.org](https://www.cisecurity.org/controls/cis-controls-list)

Når vi taler om CIS18-benchmarks, refererer det til de specifikke krav og retningslinjer, der er opstillet inden for CIS Control 18.  
Disse benchmarks kan omfatte anbefalinger vedrørende konfiguration af applikationssoftware, håndtering af adgangskontrol, implementering af sikre kodningspraksis og andre foranstaltninger for at sikre, at applikationer er robuste og mindre tilbøjelige til at blive udnyttet af trusselsaktører.
Organisationer kan bruge disse benchmarks som en vejledning til at evaluere og forbedre sikkerheden i deres applikationer i overensstemmelse med CIS Controls og dermed styrke deres samlede cybersikkerhed.  

Listen over benchmarks kan findes via linket nederst på siden. For at downloade dem skal du registrere dig hos CIS.

## Instruktioner

1. Beslut i gruppen hvilken opnsense instans i vil bruge ti at udføre hærdning. Måske vælger i at lave en klon af en eksisterende, eller i vælger måske at lave et *snapshot* af en eksisterende inden i ændrer konfigurationen.  
Under alle omstændigheder er det en god ide at overveje om den instans i arbejder på er en i får brug for senere, det kan jo være at i kommer til at ødelægge den ved en fejl?
2. Find filen **CIS_pfSense_Firewall_Benchmark_v1.1.0.pdf** på itslearning og skim den indivivuelt
3. I gruppen snak om hvilke firewall benchmarks (fra dokumentet i punkt 2) i vil implementere på routeren. Vælg så mange som muligt.
4. Priotér listen fra punkt 3 efter vigtigste tiltag først. Hvad der er det vigtigeste beslutter i selv.
5. Dokumenter jeres tiltag i gruppen gitlab projekt. Jeg regner med at se screenshots, før og efter scenarier etc. som kan understøtte og vise at i har udført det.


## Links

- [CIS18 benchmarks](https://www.cisecurity.org/cis-benchmarks)
- Jeg har fundet en hjemmeside med CIS18 kontrollerne som vedligeholdes af CIS som er let at navigere i  
[CIS Controls Assessment Specification](https://controls-assessment-specification.readthedocs.io/en/stable/index.html)