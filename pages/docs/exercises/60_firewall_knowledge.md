---
hide:
  - footer
---

# Øvelse 60 - Viden om firewalls

## Information

Øvelsen er en gruppeøvelse.

Firewalls er afgørende sikkerhedsenheder, der anvendes til at filtrere og kontrollere trafik mellem et internt netværk og det eksterne internet eller mellem forskellige segmenter af et netværk.  
Der findes forskellige typer af firewalls, og de kan filtrere trafik på forskellige måder.
I denne øvelse skal i undersøge forskellige typer af firewalls, hvad de er i stand til og hvilke svagheder de har.  

Jeg har givet et par links herunder som inspiration, men i bør selv finde yderligere ressourcer som i kan bruge til at begrunde jeres undersøgelser. Husk at være kildekritiske!

## Instruktioner

1. I jeres gruppe skal i undersøge forskellige firewall typer og hvad de kan. De forskellige typer kan være: 
    - Pakkefiltreringsfirewall
    - Stateful Inspection Firewall
    - Application Layer Firewall (Proxy Firewalls)
    - Next-Generation Firewall (NGFW)
    - Proxy Server Firewall (f.eks Web Application Firewall)
2. Lav en kort beskrivelse af hvad de forskellige typer kan, hvilket lag i OSI modellen de arbejder på og hvad deres svagheder er (hvordan de kan kompromitteres ved f.eks ip spoofing) 
3. Dokumenter jeres undersøgelse og svar i jeres gruppes gitlab projekt, jeg giver feedback næste gang. Hvis vi har tid så tager vi en kort runde på klassen baseret på jeres spørgsmål.   

## Links

- [Netwrix - [...]Common Types of Network Security Devices[...]](https://blog.netwrix.com/2019/01/22/network-security-devices-you-need-to-know-about/)
- [techtarget - 5 different types of firewalls explained](https://www.techtarget.com/searchsecurity/feature/The-five-different-types-of-firewalls)