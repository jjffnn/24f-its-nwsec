---
hide:
  - footer
---

# Øvelse 10a - Grundlæggende netværksviden **SVAR**

## Information

Dette er svar på øvelsen om [grundlæggende netværksviden](../exercises/10_basic_network_knowledge.md)

## Instruktioner

1. Hvad betyder LAN? (Local Area Network)
2. Hvad betyder WAN? (Wide Area Network)
3. Hvor mange bits er en ipv4 adresse? (32) 
4. Hvor mange forskellige ipv4 adresser findes der? (4.294.967.296) 
5. Hvor mange bits er en ipv6 adresse? (128) 
6. Hvad er en subnet maske? (32 bits der angiver hhv. netværks og host delen af en ipv4 adresse)
7. Hvor mange hosts kan der være på netværket 10.10.10.0/24 (254) (2^8-2)
8. Hvor mange hosts kan der være på netværket 10.10.10.0/22 (1022)
9. Hvor mange hosts kan der være på netværket 10.10.10.0/30 (2)
10. Hvad er en MAC adresse? (en netværksenheds fysiske adresse)
11. Hvor mange bits er en MAC adresse? (48)
12. Hvilken MAC adresse har din computers NIC? 
13. Hvor mange lag har OSI modellen ? (7)
14. Hvilket lag i OSI modellen hører en netværkshub til? (lag 1 - fysisk) 
15. Hvilket lag i OSI modellen hører en switch til? (lag 2 - data link)
16. Hvilket lag i OSI modellen hører en router til? (lag 3 - netværk)
17. Hvilken addressering anvender en switch? (MAC adresser)
18. Hvilken addressering anvender en router? (IP adresser)
19. På hvilket lag i OSI modellen hører protokollerne TCP og UDP til? (lag 4 - transport)
20. Hvad udveksles i starten af en TCP forbindelse? (3 way handshake)
20. Hvilken port er standard for SSH? (22)
21. Hvilken port er standard for https? (443)
22. Hvilken protokol hører port 53 til? (DNS)
23. Hvilken port kommunikerer OpenVPN på? (1194) 
23. Er FTP krypteret? (nej, men ftps og sftp er)
24. Hvad gør en DHCP server/service? (tildeler ip adresser dynamisk på et netværk)
25. Hvad gør DNS? (oversætter fqdn til ip adresser)
26. Hvad gør NAT? (oversætter adresser fra et netværk til et andet)
27. Hvad er en VPN? (Virtuelt netværk via en krypteret tunnel)
28. Hvilke frekvenser er standard i WIFI? (2.4 GHz og 5 GHz)
29. Hvad gør en netværksfirewall ? (fitrerer netværkstrafik)
30. Hvad er OPNsense? (mange ting, blandt andet en router og firewall)


## Links

