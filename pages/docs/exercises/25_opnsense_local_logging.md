---
hide:
  - footer
---

# Øvelse 25 - opnsense lokal overvågning

## Information

Denne øvelse skal laves som gruppe. 

opnsense giver mulighed for at overvåge netværksaktivitet direkte i web interfacet.  
Det er meget brugbart, især til fejlfinding! Dog skal det aktiveres inden der kan ses data.  

I denne øvelse skal i opsætte overvågning af DNS, netflow (netværkstrafik) og firewall i OPNsense.

Husk at klikke på det orange `i` når i arbejder, det viser hjælp direkte i GUI og hjæper med at forstå hvad det er i laver.  
Det er også vigtigt at i tager jer tid til at undersøge de ting i ikke umiddelbart forstår. Spørg hinanden, internettet eller jeres underviser!  

I bør også bruge [opnsense dokumentationen](https://docs.opnsense.org/manual) til at få mere forståelse.

I skal i en senere undervisningsgang opsætte mere overvågning af netværk og andre logs externt. Dette er starten på emnet overvågning.

## Instruktioner

1. Opsætning af dns overvågning

    opnsense bruger en lokal DNS server kaldet `unbound` som en slags DNS proxy. Det er derfor muligt at overvåge DNS aktivitet.  
    Det aktiveres i `Reporting->Unbound DNS` som vist herunder:

    ![dns reporting setup](../images/opnsense_lab_dns_enable_reporting.png)

    Efter i har aktiveret statistics kan i se hvad unbound DNS laver ved at trykke på `Unbound DNS`  
    Husk at der ikke vises noget før i laver noget DNS trafik!

2. Opsætning af lokal netflow

    Netflow er en CISCO protokol til at logge netværkstrafik. Den findes i flere versioner hvor V9 i dag er standarden. V5 bruges dog stadig på ældre netværksudstyr. Netflow giver informationer om source og destinations ip, port samt meget andet.  
    I kan læse om netflow her [Cisco netflow whitepaper](https://www.cisco.com/en/US/technologies/tk648/tk362/technologies_white_paper09186a00800a3db9.html) 

    Det aktiveres i `Reporting->Netflow` som vist herunder:

    ![opnsense netflow setup](../images/opnsense_lab_netflow_setup.png)

    Efter det er aktiveret kan i se netflow trafik i `insight`

    ![netflow insight](../images/opnsense_lab_netflow_insight.png)

    Undersøg hvad i kan få vist i `insight` ved at være nysgerrige og klikke på de forskellige muligheder i interfacet.

3. Firewall live view

    Det er også muligt at se hvad de forskellige firewalls laver, det ses live fra `Firewall->Log FIles->Live View`

    ![](../images/opnsense_lab_firewall_liveview.png)

    Afprøv de forskellige filter muligheder i dropdown felterne `action` `contains` `pass` for at lære dem at kende.  

    Kan i finde andre måder at se firewall aktivitet, er der nogen andre views?

## Links

- [opnsense dokumentationen](https://docs.opnsense.org/manual)
- [what is DNS (cloudflare)](https://www.cloudflare.com/learning/dns/what-is-dns/)
- [unbound dns](https://www.nlnetlabs.nl/projects/unbound/about/)
- [NetFlow Basics: An Introduction to Monitoring Network Traffic (auvik)](https://www.auvik.com/franklyit/blog/netflow-basics/)
- [NetFlow for Cybersecurity (cisco)](https://www.ciscopress.com/articles/article.asp?p=2812391&seqNum=5)