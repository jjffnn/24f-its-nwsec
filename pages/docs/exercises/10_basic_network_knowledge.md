---
hide:
  - footer
---

# Øvelse 10 - Grundlæggende netværksviden

## Information

Denne øvelse skal laves som gruppe. Meningen er at gruppen bliver enige om nedenstående spørgsmål.  
På klassen laver vi opsamling og vidensdeling efter øvelsen.   
Det anbefales at i dokumenterer jeres fælles svar på gitlab i eftermiddag.

## Instruktioner

Besvar følgende:  

1. Hvad betyder LAN? 
2. Hvad betyder WAN? 
3. Hvor mange bits er en ipv4 adresse? 
4. Hvor mange forskellige ipv4 adresser findes der? 
5. Hvor mange bits er en ipv6 adresse?  
6. Hvad er en subnet maske? 
7. Hvor mange hosts kan der være på netværket 10.10.10.0/24 
8. Hvor mange hosts kan der være på netværket 10.10.10.0/22 
9. Hvor mange hosts kan der være på netværket 10.10.10.0/30 
10. Hvad er en MAC adresse? 
11. Hvor mange bits er en MAC adresse? 
12. Hvilken MAC adresse har din computers NIC? 
13. Hvor mange lag har OSI modellen ? 
14. Hvilket lag i OSI modellen hører en netværkshub til? 
15. Hvilket lag i OSI modellen hører en switch til? 
16. Hvilket lag i OSI modellen hører en router til? 
17. Hvilken addressering anvender en switch? 
18. Hvilken addressering anvender en router? 
19. På hvilket lag i OSI modellen hører protokollerne TCP og UDP til? 
20. Hvad udveksles i starten af en TCP forbindelse? 
20. Hvilken port er standard for SSH? 
21. Hvilken port er standard for https? 
22. Hvilken protokol hører port 53 til? 
23. Hvilken port kommunikerer OpenVPN på? 
23. Er FTP krypteret? 
24. Hvad gør en DHCP server/service? 
25. Hvad gør DNS? 
26. Hvad gør NAT? 
27. Hvad er en VPN? 
28. Hvilke frekvenser er standard i WIFI? 
29. Hvad gør en netværksfirewall ? 
30. Hvad er OPNsense? 

## Links
