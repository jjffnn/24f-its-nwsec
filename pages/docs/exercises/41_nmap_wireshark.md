---
hide:
  - footer
---

# Øvelse 41 - Nmap wireshark

## Information

Dette er en gruppeøvelse.  

I øvelsen [Nmap Basic Port Scans](../exercises/40_thm_nmap.md) lærte i NMAP at kende.  
Formålet med øvelsen er at lære NMAP lidt bedre at kende og samtidig bruge wireshark til at observere hvordan netværkstrafikken som NMAP laver ser ud.  
Øvelsen giver også et indblik i hvordan NMAP kan bruges til at teste sikkerhed i netværksopsætning.  

## Instruktioner

1. åbn Kali på proxmox
2. åbn wireshark og vælg `eth0` som det interface i sniffer trafik fra
3. åbn en terminal, naviger til Documents mappen og lav en ny mappe som hedder `Nmap scans` som du kan bruge til at gemme nmap output
4. kør en nmap scanning i terminalen med kommandoen `nmap -sC -v -oA nmap_scan 192.168.1.1` 
5. Hvilken host er det i scanner?
5. Hvilke porte er åbne?
6. Hvilke protokoller og services kører på de åbne porte?
7. Hvordan ser trafikken ud i wireshark? (filtrer på den skennede host) 
8. Gem trafikken fra wireshark som en `.pcapng` fil i `Nmap scans` mappen
7. Åbn endnu en terminal og start en webserver med `sudo python3 -m http.server 8000`
8. Åbn en browser på adressen `http://127.0.0.1:8000` - hvad ser du?  
8. kør en nmap scanning i terminalen med kommandoen `nmap -sC -v -oA nmap_scan_webserver 192.168.1.100`
5. Hvilken host er det i scanner?
5. Hvilke porte er åbne?
6. Hvilke protokoller og services kører på de åbne porte?
7. Hvordan ser trafikken ud i wireshark? (filtrer på den skennede host) 
8. Gem trafikken fra wireshark som en `.pcapng` fil i `Nmap scans` mappen
9. **Sluk for webserveren** og kør `nmap -sC -v -oA nmap_scan_webserver_2 192.168.1.100`
10. Er der nogle porte som er åbne nu? Hvorfor ikke?

## Links

- [Nmap Reference Guide](https://nmap.org/book/man.html)