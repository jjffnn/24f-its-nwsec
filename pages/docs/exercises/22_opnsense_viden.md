---
hide:
  - footer
---

# Øvelse 22 - opnsense viden 1

## Information

Denne øvelse skal laves som gruppe. Meningen er at gruppen bliver enige om nedenstående spørgsmål.  
Dokumenter jeres svar på gitlab. 

## Instruktioner

1. Hvilken ip version tildeles som default til LAN og WAN interface ved installation?
2. Hvilket OS er opnsense baseret på?
3. Hvad betyder `live mode` i opnsense? 
4. Hvilket interface og ip adresse kan opnsense webinterfacet tilgås fra? 

## Links

