---
hide:
  - footer
---

# Øvelse 54 - Implementering af netværksdesign

## Information

Dette er en gruppe øvelse.

Formålet med øvelsen er at få rutine i at **implementere** sikre segmenterede netværk.
Det vil være nødvendigt at kunne som en del af eksamensprojektet og det er også en god øvelse i at få rutine med grundlæggende netværk.

Opgaven er stor og tager det meste af dagen. Jeg har gjort mit bedste for at kommunikere hvad der skal opsættes og forsøgt at gøre det så udførligt som muligt.  
Når det er sagt så husk at i er studerende, hvilket kræver at i selv er nysgerrige på de ting i ikke forstår. Det betyder ikke at i er alene og jeg vil kraftigt opfrodre til at i bruger hinanden, også imellem studiegrupper, internettet og jeres underviser.  

Jo mere nysgerrige i er og jo mere i går på opdagelse i emner, begreber og teknologier, jo mere helhedsforståelse får i for de emner vi arbejder med i faget.  

Det modsatte kan siges hvis jeres tilgang er at skulle være færdige så hurtigt som muligt, eller hvis i ikke bruger tid på at nå i mål. Så vil læringen være mindre og i vil gå glip af en masse (synes jeg) super spændende ting!

Der vil være ting i denne øvelse som driller og frustrerer, det var der også for mig da jeg lavede øvelsen til jer.  
Derfor vil der også blive rig mulighed for at fejlfinde!  
Når i fejlfinder kan i bruge disse tip:

**Fejlfindings tip 1:** Brug `Firewall->Log Files` i OPNsense for at se hvad firewallen gør.  
**Fejlfindings tip 2:** Brug `Reporting` i OPNsense for at se netflow og andre logs.    
**Fejlfindings tip 3:** Brug wireshark på Kali maskinen til at se trafik (husk i kan tilslutte den til andre netværk).  

I forhold til dokumentation af denne opgave skal i forestille jer at i er ansat i en virksomhed der har fået denne opgave fra en kunde.  

Kunden har efterspurgt dokumentation i form af netværksskanninger, ping osv. som bevis for at implementationen følger designet.  
Så i skal ikke dokumentere det samme som jeg beskriver herunder, i skal dokumentere at det virker som beskrevet.  

Derfor er det vigtigt at i overvejer, hvordan det er hensigtsmæssigt at vise om for eksempel firewall reglerne virker. 

**Eksempel 1:** Hvis i kører en nmap scanning fra DMZ til management, er alle porte så lukkede?  
**Eksempel 2:** Hvad kan i pinge fra de forskellige netværk, suppleret med screenshots fra OPNSense firewall live view.  
**Eksempel 3:** Screenshots af opsætningen af firewall etc. 

I må også meget gerne notere gode forslag og andet feedback til designeren hvis i har noget der er konstruktivt :-)
 
Netværksdesignet der skal **implementeres** er dokumenteret herunder, som et diagram og som beskrivelse af firewall regler.  
Firewall regler følger denne skabelon og er yderligere beskrevet med et screenshot fra min implementation (it works on my machine):

```md
| Action | TCP/IP Version | Protocol | Source | Dest / Invert | Destination | Dest Port | Description |
| :----- | :------------- | :------- | :----- | :------------ | :---------- | :-------- | :---------- |
| Pass   | IPv4           | TCP/UDP  |        | unchecked     |             |           |             |
```

**Bemærk!** Den 2. octet i jeres netværksadresser skal erstattes med host delen af jeres proxmox maskines ip, f.eks bliver 10.56.16.**31** til 10.**31**.10.0/24, 10.31.20.0/24 osv.  
Grunden er at vi alle er på det samme netværk på den anden side af WAN. (Ja vi burde have en kørende IPAm løsning, kender i en god en??)

![](../images/lab_diagram_20240301.png)  
_Lab diagram 20240301 - NISI_


## 1. OPNsense VM opsætning

Hardware er en virtuel maskine der kører OPNsense: 

1. Konfigurer VM som nedenstående - Læg mærke til at der er opsat flere `bridge` i proxmox. Det kan gøres på proxmomx noden:
    
    **vmbr config på proxmox noden**  
    ![vmbr config](../images/opnsense_lab_proxmox_vmbrconfig.png)

    **OPNsense HW config**  
    ![OPNsense config](../images/opnsense_lab_hardware.png)

2. Konfigurer `assignments` og `interfaces` på OPNsense VM (det kan være en fordel at gøre for hhv. WAN og første interface fra CLI på OPNsense VM, derefter fra OPNsense webinterfacet)  
Husk at konfigurere DHCP på jeres netværk, det har i gjort før under `Services->ISC DHCPv4`

    ![OPNsense assignments](../images/opnsense_lab_assignments.png)  

    ![wan config](../images/opnsense_lab_wan_config.png)  

    ![management config](../images/opnsense_lab_mgmt_config.png)  

    ![dmz config](../images/opnsense_lab_dmz_config.png)  

    ![monitoring config](../images/opnsense_lab_monitor_config.png)  

## 2. Kali netværksopsætning

1. Konfigurer netværk på kali maskinen. Herunder vist med udgangspunkt i at forbinde til management netværket:

    ![](../images/opnsense_lab_kali_nw_config.png)

2. Brug kali maskinen til at forbinde til hvert netværk, kan i få en ip adresse? Hvis ikke så fejlsøg.
3. Kør en NMAP scanning mod de forskellige netværk så i ved hvilke porte der er åbne. Gem NMAP output og noter resultater i dokumentationen.

## 3. Opsætning af ALIAS i OPNsense

Opsæt nedenstående ALIAS i OPNsense firewall

**Private networks**  
![priv nw alias](../images/opnsense_privnw_alias.png)

**VPLE ports**  
![vple ports](../images/opnsense_VPLE_alias.png)

## 4. Firewall opsætning i OPNsense

Herunder er angivet hvordan firewalls på de enkelte netværk slal konfigureres. I jeres gruppe bør i tale om hvad de forskellige regler gør så i forstår dem. Tal også om de er tilstrækkelige eller for brede. Altså om de giver adgang til for meget eller for lidt, hvad synes i?  

Tag pointer fra jeres samtaler med i jeres dokumentation.  

Jeg har lært meget af de forskellige artikler og videoer fra [Home Network Guy](https://homenetworkguy.com/categories/firewalls/) og [OPNsense dokumentationen](https://docs.opnsense.org/firewall.html)

### 00WAN firewall regler:

Intet konfigureret udover autogenererede regler. Det betyder at der ikke er nogle porte åbne fra WAN og ind i OPNsense.
Hvis i mener der er behov for det så kan i undersøge hvad der er konfigureret og dokumentere det.

![](../images/opnsense_WAN_FW_rules.png)

### 01MANAGEMENT firewall regler

Formålet med management netværket er at have et netværk der kan bruges til at konfigurere OPNsense og andre enheder på de forskellige netværk.  
Det er dette netværk kun administratorer kan tilgå, det vil sige et meget begrænset antal mennesker fordi adgang til dette netværk giver mulighed for at kontrollere alt andet på netværket.  

| Action | TCP/IP Version | Protocol | Source     | Dest / Invert | Destination      | Dest Port    | Description                                  |
| :----- | :------------- | :------- | :--------- | :------------ | :--------------- | :----------- | :------------------------------------------- |
| Pass   | IPv4+6         | TCP/UDP  | 01MGMT net | unchecked     | MGMT address     | 53 (DNS)     | Allow access to DNS                          |
| Block  | IPv4           | TCP/UDP  | 01MGMT net | unchecked     | \*               | 53 (DNS)     | Block access to external DNS                 |
| Pass   | IPv4+6         | TCP/UDP  | 01MGMT net | unchecked     | MGMT address     | 443 (HTTPS)  | Allow access to OPNsense web UI              |
| Pass   | IPv4           | TCP/UDP  | 01MGMT net | unchecked     | 10.XX.20.100     | VPLEPorts    | Allow access to VPLE host exposed ports      |
| Pass   | IPv4           | TCP/UDP  | 01MGMT net | unchecked     | 10.XX.30.10      | GraylogPorts | Allow access to graylog on portainer         |
| Pass   | IPv4           | TCP/UDP  | 01MGMT net | unchecked     | 10.XX.30.10      | 9443         | Allow access to PORTAINER host exposed ports |
| Pass   | IPv4           | ICMP     | 01MGMT net | unchecked     | \*               | \*           | Allow ICMP (PING)                            |
| Pass   | IPv4           | TCP/UDP  | 01MGMT net | checked       | !PrivateNetworks | \*           | Allow access only to public networks         |

![](../images/opnsense_MGMT_FW_rules.png)

### 02DMZ firewall regler

DMZ står for Demilitarized Zone og er et begreb der stammer fra ingenmandslandet mellem 2 lande som er i konflikt.  
Det var der mellem øst og vesttyskland inden muren faldt og det ses også mellem nord og sydkorea.  

I netværkssammenhæng er det et netværk der har porte eksponeret mod omverdenen og her webservere, mail servere og andre services der skal kunne tilgås fra WAN (læs internettet). DMZ befinder sig i netværket mellem WAN firewallen og en eller flere andre firewalls.  
Der vil som regel være lukket for trafik fra DMZ netværket mod de øvrige netværk fordi det er et sted der er eksponeret og dermed har større mulighed for at blive brugt som en angrebsvektor.   

Der er flere forskellige tilgange til at lave en DMZ i netværket - læs denne artikel for at få mere viden om DMZ:  
[How to Create a Basic DMZ](https://homenetworkguy.com/how-to/create-basic-dmz-network-opnsense/)

| Action | TCP/IP Version | Protocol | Source     | Dest / Invert | Destination      | Dest Port    | Description                                  |
| :----- | :------------- | :------- | :--------- | :------------ | :--------------- | :----------- | :------------------------------------------- |
| Pass   | IPv4+6         | TCP/UDP  | 02DMZ net  | unchecked     | 02DMZ address    | 53 (DNS)     | Allow access to DNS                          |
| Block  | IPv4           | TCP/UDP  | 02DMZ net  | unchecked     | \*               | 53 (DNS)     | Block access to external DNS                 |
| Pass   | IPv4           | TCP/UDP  | 02DMZ net  | checked       | !PrivateNetworks | \*           | Allow access only to public networks         |

![](../images/opnsense_DMZ_FW_rules.png)

### 03MONITORING firewall regler

Monitoring netværket er endnu ikke i brug, formålet med netværket er at kunne placere forskellige monitorerings services som for eksempel wazuh og graylog.  
Ideen i at have dem adskilt er at de vil være vigtige datakilder i tilfælde af et angreb og det efterfølgende opklaringsarbejde.  
Derfor er der det ekstra sikkerhedslag foran dette netværk med henblik på at udelukke angribere der har adgang til for eksempel `DMZ` netværket.  
Center for cybersikkerhed har skrevet en god vejledning til logning so i bør orientere jer i for at få inspiration til hvor i kan implementere logning i eksamensprojektet.  
[Logning - en del af et godt cyberforsvar (CFCS)](https://www.cfcs.dk/da/forebyggelse/vejledninger/logning/)

På monitoring firewallen er intet konfigureret, udover autogenererede regler. Hvis i mener der er behov for det så kan i undersøge hvad der er konfigureret og dokumentere det.

![](../images/opnsense_MONITOR_FW_rules.png)

## 6. VPLE netværksopsætning

Der er endnu ingen services der kører på DMZ segmentet. Her vil i en anden undervisningsgang i system sikkerhed sætte f.eks en webserver op. Jeg vil pege på en gammel kending [VPLE - Vulnerable pentesting lab environment](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) som i alle har arbejdet med tidligere.  

Det passer også med at i allerede har opsat firewall regler på `MGMT` interfacet til at tilgå de forskellige VPLE services (DVWA, Juiceshop etc.) fra det netværk.   

Det giver mig samtidig mulighed for at vise jer hvordan i kan importere virtuelle maskine i `.ova` format fra vmware workstation til proxmox (very kool...).  
Jeg har selv gjort det flere gange og beskrevet det på min blog, som er den guide i skal følge.  

1. Importer VPLE ova til proxmox, guide her: [jalokin.com/proxmox/proxmox_convert_ovf/](https://jalokin.com/proxmox/proxmox_convert_ovf/)
2. I leases på opnsense dhcp serveren kan i finde VPLE maskinen og give den statisk IP ifølge netværksdiagrammet (kig efter det lille + ud for VPLE maskinen).  
OBS! Hvis du rebooter VPLE skal du køre `sudo dhclient -r` efterfulgt af `sudo dhclient` på VPLE maskinen igen (som skrevet i guiden), den henter ikke selv en IP fra DHCP ved reboot, heller ikke når den er statisk konfigureret.  
   Måske er der en smart måde at få den til det, men jeg kender den desværre ikke :-/
    ![](../images/opnsense_lab_dhcp_vple_static.png)


## 5. Begræns opnsense GUI adgang

Per default er der adgang til GUI på OPNsense fra alle netværksadresser, derfor er dette er et vigtigt punkt, ved at begrænse adgangen fra andre netværk end `management` vil det være mere kompliceret, for en trusselsaktør at ændre på netværkskonfigurationen.  

Til gengæld er der også mulighed for at i lukker jer selv ude af management interfacet og dermed skal lave alt på ny. Så tænk det igennem inden i gør det og husk evt. at tage et snapshot af jeres OPNsense VM før i gør dette!   

![GUI skal kun lytte på MGMT netværket](../images/opnsense_listen_GUI_SSH.png)

## Links

- Kig i øvelser fra de foregående uger, der er mange ressourcer som i kan bruge til denne øvelse.
