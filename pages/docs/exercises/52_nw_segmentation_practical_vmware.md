---
hide:
  - footer
---

# Øvelse 52 - Praktisk netværkssegmentering i vmware workstation med opnsense

## Information

Øvelsen er individuel.

I denne øvelse skal du arbejde praktisk med segmentering af netværk i opnsense.  
Formålet er at lære hvordan du kan udvide opnsense med et ekstra netværksinterface og tilhørende netværk.

## Instruktioner

1. Sluk for opnsense VM. Tilføj et ekstra netværksinterface på samme måde som da du konfigurerede maskinen i [Øvelse 20 - OPNsense på vmware workstation](../exercises/20_opsense_vmware.md) 
2. Tilslut netværksinterfacet til vmnet3.     
3.  Tænd for opnsense og konfigurer det nye interface som nedenstående:  

    _Assign nyt interface, kald det `management`_  
    ![Assign nyt interface](../images/opnsense_new_network/opnsense_new_network_1.png)  

    _Konfigurer interfacet som følgende_   
    ![](../images/opnsense_new_network/opnsense_new_network_2.png)   
    ![](../images/opnsense_new_network/opnsense_new_network_3.png)
    ![](../images/opnsense_new_network/opnsense_new_network_4.png)
    ![](../images/opnsense_new_network/opnsense_new_network_5.png)  
    
    _Tænd for DHCP på netværket og tildel følgende DHCP range_    
    ![](../images/opnsense_new_network/opnsense_new_network_6.png)    
    ![](../images/opnsense_new_network/opnsense_new_network_10.png)

4. Tilslut en ny VM til vmnet3 og bekræft at maskinen får tildelt en IP i den ip range du har konfigureret på interfacet.
    ![](../images/opnsense_new_network/opnsense_new_network_9.png)
    ![](../images/opnsense_new_network/opnsense_new_network_11.png)



## Links

- [How to Create a Basic DMZ (Demilitarized Zone) Network in OPNsense](https://homenetworkguy.com/how-to/create-basic-dmz-network-opnsense/)
- [OPNsense docs - Interfaces](https://docs.opnsense.org/interfaces.html)
