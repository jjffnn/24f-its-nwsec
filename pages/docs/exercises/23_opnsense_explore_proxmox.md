---
hide:
  - footer
---

# Øvelse 23 - opnsense udforskning

## Information

Denne øvelse skal laves som gruppe. 
Formålet med øvelsen er i får et overblik over hvad opnsense er og hvad det kan udover at være en router.  
Husk at dokumentere jeres svar på gitlab på en måde så andre kan bruge det. (screenshots og links er ok, husk at lave lidt tekst som supplement til screenshots)

## Instruktioner

Gennemgå dokumentationen for opnsense på [https://docs.opnsense.org/](https://docs.opnsense.org/) samtidig med at i finder tingene på jeres opnsense installation i proxmox.  
Besvar følgende:  

1. Hvilke features tilbyder opnsense? 
1. Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder? 

1. Hvad er lobby og hvad kan man gøre derfra? 
1. Kan man se netflow data direkte i opnsense og i så fald hvor kan man se det? 
1. Hvad kan man se omkring trafik i `Reporting`? 
1. Hvor oprettes vlan's og hvilken IEEE standard anvendes? 
1. Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?
1. Hvilke slags VPN understøtter opnsense? 
1. Hvilket IDS indeholder opnsense?
1. Hvor kan man installere `os-theme-cicada` i opnsense? 
1. Hvordan opdaterer man firmware på opnsense?
1. Hvad er nyeste version af opnsense? 
1. Er jeres opnsense nyeste version? Hvis ikke så opdater den til nyeste version :-)
    

## Links

- Home network guy artikel [Beginner's Guide to Set Up a Home Network Using OPNsense](https://homenetworkguy.com/how-to/beginners-guide-to-set-up-home-network-using-opnsense/)
- Home network guy artikel [12 Ways to Secure Access to OPNsense and Your Home Network](https://homenetworkguy.com/how-to/ways-to-secure-access-to-opnsense-and-your-home-network/)
- Home network guy artikel video playliste [Set up a Full Network using OPNsense](https://youtube.com/playlist?list=PLZeTcCOrKlnDlyZCIxhFZukAnA0NNWL_I&si=HJTg7AshyBXFHQbB)
