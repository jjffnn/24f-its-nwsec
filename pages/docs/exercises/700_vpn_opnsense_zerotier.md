# OPNsense zerotier guide - first install

## Guide

(OPNsense documentation)[https://docs.opnsense.org/manual/how-tos/zerotier.html]

## Videoguide

(Sooo... OPNsense does Zerotier!!! Basic setup guide)[https://youtu.be/qPgcH6T9Tfo?si=--8_DFxfzUCRveXG]

## Plugin install output

***GOT REQUEST TO INSTALL***
Currently running OPNsense 24.1.1 at Sun Feb 18 13:05:39 CET 2024
Updating OPNsense repository catalogue...
OPNsense repository is up to date.
All repositories are up to date.
The following 2 package(s) will be affected (of 0 checked):

New packages to be INSTALLED:
	os-zerotier: 1.3.2_4
	zerotier: 1.12.2

Number of packages to be installed: 2

The process will require 2 MiB more space.
616 KiB to be downloaded.
[1/2] Fetching os-zerotier-1.3.2_4.pkg: .. done
[2/2] Fetching zerotier-1.12.2.pkg: .......... done
Checking integrity... done (0 conflicting)
[1/2] Installing zerotier-1.12.2...
[1/2] Extracting zerotier-1.12.2: ......... done
[2/2] Installing os-zerotier-1.3.2_4...
[2/2] Extracting os-zerotier-1.3.2_4: .......... done
Stopping configd...done
Starting configd.
Migrated OPNsense\Zerotier\Zerotier from 0.0.0 to 1.3.0
Reloading plugin configuration
Configuring system logging...done.
Reloading template OPNsense/zerotier: OK
=====
Message from zerotier-1.12.2:

--
Note that ZeroTier 1.4.6+ has a *new* license prohibiting commercial SaaS
style usage, as well as excluding government organisations. Read the
license details carefully to ensure your compliance.

First start the zerotier service:

service zerotier start

To connect to a zerotier network:

zerotier-cli join <network>

If you are running other daemons or require firewall rules to depend on
zerotier interfaces being available at startup, you may need to enable
the following sysctl in /etc/sysctl.conf:

net.link.tap.up_on_open=1

This avoids a race condition where zerotier interfaces are created, but
not up, prior to firewalls and services trying to use them.

You can place optional configuration in /var/db/zerotier-one/local.conf
as required, see documentation at https://www.zerotier.com/manual.shtml

If your system boots from DHCP (such as a laptop), there is a new rc.conf
flag that will require that system startup will wait until the zerotier
network is established before proceeding. Note that this flag *does not*
work for systems configured with statically assigned IP addresses, and
these will hang indefinitely due to an irreducible loop in rc(8) startup
files. This flag is disabled by default.
Checking integrity... done (0 conflicting)
Nothing to do.
***DONE***