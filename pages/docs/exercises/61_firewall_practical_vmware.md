---
hide:
  - footer
---

# Øvelse 61 - Konfigurering af firewall i opnsense

## Information

Øvelsen er individuel.

En forudsætning for denne øvelse er at du har opnsense konfigureret med 3 interfaces og 2 interne netværk som beskrevet i [Øvelse 52 - Praktisk netværkssegmentering i vmware workstation med opnsense](../exercises/52_nw_segmentation_practical_vmware.md)

På hvert af de interne netværk skal du have en vm tilsluttet. Den ene skal have python3 installeret og den anden skal have nmap installeret.  
Maskinen med nmap kan for eksempel være en kali maskine, den anden er valgfri.  
Det er klart at du kommer hurtigere igang med øvelsen hvis du genbruger eksisterende virtuelle maskiner du allerede har installeret.  

Dit setup bør se ud som:  

![Firewall test netværk](../images/firewall_test_setup.drawio.png)

Firewall reglerne i opnsense konfigureres på det netværk som trafikken kommer fra. Det kan gøres anderledes men det er den måde opnsense anbefaler. Det vil sig at du hvis du vil tillade trafik fra `LAN` til et eller flere andre netværk skal du konfigurere firewallen med `LAN` som source.  

Flowet i firewallen kan derfor betragtes som nedenstående illustration:

![Firewall rule flow](../images/firewall_rule_flow.png)  
_Mere info: [opnsense docs - Firewall rules](https://docs.opnsense.org/manual/firewall.html#rules)_  

Det kan tage lidt tid at vænne sig til og den opmærksomme vil også undre sig over at firewallen på modtager netværket, accepterer trafikken og ikke skal konfigureres yderligere. Det er dog forklaret i [dokumentationen](https://docs.opnsense.org/manual/firewall.html#direction) _"Traffic leaving the firewall is accepted by default (using a non-quick rule), when Disable force gateway in Firewall ‣ Settings ‣ Advanced is not checked, the connected gateway would be enforced as well."_  

Ud over det er det vigtigt at bemærke at regler eksekveres sekventielt fra toppen af listen med regler, hvis en regel matcher trafikken bruges reglen og efterfølgende regler afvikles ikke.  
_Eksempel_ Den første regel blokerer TCP trafik fra port 80 på `LAN` til hele `management` netværket. Den efterfølgende regel tillader alt trafik fra `LAN` til `management`.  
Hvis en host på `LAN` kommunikerer TCP trafik på port 80 vil trafikken blive blokeret af den første regel og dermed ikke sendt videre. Hvis hosten på `LAN` derimod kommunikerer TCP på en anden port _passerer_ trafikken den første regel og fortsætter til sin destination på `management` netværket.  

Der er forskellige tilgange til at lave firewall regler og det er muligt at være mere eller mindre specifik i sine firewall regler. En meget specifik regel kan for eksempel gælde for en enkelt ip adresse, på en enkelt port med en enkelt protokol. Mindre specifikke regler kan gælde en enkelt protokol op til en række af porte, ja op til et helt netværk og alle porte samt protokoller.   
Som tommelfinger regel bør de mest specifikke regler være øverst i reglerne og de efterfølgende være mere generelle (hvis der er behov for generelle regler overhovedet?)  

En tilgang/strategi til firewall regler er, kun at åbne for trafik du vil tillade der forlader netværket, implementeret med specifikke regler (husk at der pr. default kun er åbent for den trafik som der er konfigureret regler for) men det kræver at du ved præcist, hvilken trafik du forventer der kommer fra enhederne på netværket. Det vil typisk være muligt med for eksempel en webserver eller andre specifikke tjenester.  
Den strategi kan kaldes en "allow list" strategi.   

På netværk hvor der er mange "brugere" med laptops, telefoner osv. vil der potentielt være mange regler du skal ladministrere. Her kan en bedre tilgang være at lukke for specifik trafik og have en "catch all" regel til sidst, der tillader alt øvrigt trafik. Den strategi kan man kalde en "deny list" strategi.  

Det kan være lidt tricky at forstå hvordan firewall reglerne fungerer. Den bedste måde at forstå det på, er at afprøve det selv, hvilket er formålet med denne øvelse.    

I bunden af øvelsen har jeg lagt et par links jeg har brugt til at forstå hvordan det virker.  
Jeg anbefaler at du orienterer dig i dokumentationen inden du begynder at arbejde, så ved du bedre hvad det er du laver ;-)

Et lille tip inden du går i gang, husk at trykke på knappen `Apply Changes` når du har rettet en regel i en firewall.

## Instruktioner

### Del 1 - setup

1. Find ud af hvilke ip adresser dine hosts har fået tildelt af dhcp. Kontroller at de er på hver sit netværk. Du kan bruge kommandoen `ip a` i en terminal på linux til at se din ip adresse.
2. Prøv at pinge fra hhv. den ene og den anden host. Kan du få ping igennem begge veje? Hvis ikke så er der nok en firewall der blokerer noget. Hvor kan du ikke pinge fra?
3. Grunden til at du ikke kan pinge fra `management` netværket er at firewallen i opnsense pr. default er sat til at blokere alt trafik fra netværket og ud.  
Du kan sammenligne reglerne der er på `LAN` netværket. Her er der 2 regler der tillader hhv. ipv4 og ipv6 trafik fra `LAN` til `any`, altså til alle andre netværk.
4. Lav en `allow all` regel i firewallen på `management` der tillader alt ipv4 trafik fra `management` netværket til `any`.  
Det er samme regel som der findes på `LAN` (faktisk kan du clone/kopiere reglen fra `LAN` til `managemnet` men det kan du selv lede efter i linksne herunder)  
5. Kontroller at du nu kan pinge hosten på `LAN`
6. Kør en NMAP scanning fra hosten på `LAN` til hosten på `management`. Brug `nmap -sC -sV <host-ip>`.  Hvor mange porte er åbne? 
7. Start en web server på `management hosten` brug `sudo python3 -m http.server 80`. 
8. Kør en NMAP scanning fra hosten på `LAN` til hosten på `management` igen. Nu burde NMAP vise at port 80 er åben og at det er `SimpleHTTPServer` der kører som service. 
    ```
    Nmap scan report for 10.10.10.201
    Host is up (0.0067s latency).
    Not shown: 999 filtered tcp ports (no-response)
    PORT   STATE SERVICE VERSION
    80/tcp open  http    SimpleHTTPServer 0.6 (Python 3.11.2)
    | http-methods: 
    |_  Supported Methods: GET HEAD
    |_http-title: Directory listing for /
    |_http-server-header: SimpleHTTP/0.6 Python/3.11.2
    ``` 

### Del 2 - Træn opsætning af firewall regler

1. Lav en regel der blokerer TCP trafik på port 80 fra `LAN` til `management`, reglen skal være over `allow all` reglen fra del 1 - trin 4. Test med NMAP og bekræft at webserveren ikke kan ses i NMAP resultatet.
2. Genstart webserveren på `management hosten` med `sudo python3 -m http.server 443`. Test med NMAP og bekræft at webserveren nu kan ses i NMAP resultatet på port 443.
3. Lav en ny regel der blokerer **UDP** trafik på port 443 fra `LAN` til `management`. Test med NMAP, kan du se webserveren på port 443?
4. Ret din UDP regel til TCP, test med NMAP. Test med NMAP, kan du se webserveren på port 443?
5. Deaktiver reglen der blokerer TCP trafik på port 80 fra `LAN` til `management`. (den fra del 2 - trin 1)
6. Åbn endnu en terminal på `management hosten` og start enndnu en webserver med `sudo python3 -m http.server 80`. Du bør nu have 2 webservere kørende, en på port 443, en anden på port 80. Test med NMAP, hvad der er åbent på `management hosten` ?
7. Sluk for begge webservere og test med NMAP. Hvor mange porte er åbne på `management hosten` ? Hvorfor er port 80 `closed`? Du har jo en `allow all` regel?
8. Deaktiver din `allow all` regel, test med NMAP. Er port 80 stadig `closed`?

Det var en meget kort introduktion til firewall regler.  
Perspektivert er selvfølgelig at du selv undersøger videre herfra med henblik på at implementere mange flere firewall regler i eksamensprojektet.
 
## Links

- [How to Configure Firewall Rules in OPNsense](https://homenetworkguy.com/how-to/configure-opnsense-firewall-rules/)
- [opnsense docs - Firewall rules](https://docs.opnsense.org/manual/firewall.html#rules)
- [restart networking linux](https://www.cyberciti.biz/faq/linux-restart-network-interface/)