---
hide:
  - footer
---

# Øvelse 23a - opnsense udforskning **SVAR**

## Information

Denne øvelse skal laves som gruppe. 
Formålet med øvelsen er i får et overblik over hvad opnsense er og hvad det kan udover at være en router.  
Husk at dokumentere jeres svar på gitlab!

## Instruktioner

1. Gennemgå dokumentationen for opnsense på [https://docs.opnsense.org/](https://docs.opnsense.org/) samtidig med at i finder tingene på jeres opnsense installation i proxmox.  
Besvar følgende:  
    1. Hvilke features tilbyder opnsense? (https://docs.opnsense.org/intro.html#opnsense-core-features)
    1. Hvordan kan du kontrollere om din opnsense version har kendte sårbarheder? (kør audit https://docs.opnsense.org/security.html#staying-ahead)
    1. Hvad er nyeste version af opnsense? (24.1 "Savvy Shark" https://docs.opnsense.org/releases.html)
    1. Hvad er lobby og hvad kan man gøre derfra? (https://docs.opnsense.org/manual/gui.html)
    1. Kan man se netflow data direkte i opnsense og i så fald hvor kan man se det? (https://docs.opnsense.org/manual/netflow.html#netflow-analyzer-insight)
    1. Hvad kan man se omkring trafik i `Reporting`? (mængde, top talkers)
    1. Hvor oprettes vlan's og hvilken IEEE standard anvendes? (interfaces->other types->VLAN og IEEE 802.1Q)
    1. Er der konfigureret nogle firewall regler for jeres interfaces (LAN og WAN)? Hvis ja, hvilke?
    1. Hvilke slags VPN understøtter opnsense? (https://docs.opnsense.org/manual/vpnet.html)
    1. Hvilket IDS indeholder opnsense?
    1. Hvor kan man installere `os-theme-cicada` i opnsense? (system->firmware->plugins)
    1. Hvordan opdaterer man firmware på opnsense?
    1. Er jeres opnsense nyeste version? Hvis ikke så opdater den til nyeste version :-)
    

## Links

- Home network guy artikel [Beginner's Guide to Set Up a Home Network Using OPNsense](https://homenetworkguy.com/how-to/beginners-guide-to-set-up-home-network-using-opnsense/)
- Home network guy artikel [12 Ways to Secure Access to OPNsense and Your Home Network](https://homenetworkguy.com/how-to/ways-to-secure-access-to-opnsense-and-your-home-network/)
- Home network guy artikel video playliste [Set up a Full Network using OPNsense](https://youtube.com/playlist?list=PLZeTcCOrKlnDlyZCIxhFZukAnA0NNWL_I&si=HJTg7AshyBXFHQbB)
