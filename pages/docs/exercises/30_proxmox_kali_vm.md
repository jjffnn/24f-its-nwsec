---
hide:
  - footer
---

# Øvelse 30 - Kali Linux på proxmox

## Information

Kali Linux er en debian baseret Linux distribution der har mange værktøjer til at arbejde med både offensiv og defensiv sikkerhed.

Vi skal bruge Kali meget i faget og derfor hjælpe denne øvelse med at få installeret det på proxmox.

Dette er en gruppeøvelse.  

## Instruktioner

1. Log ind på proxmox
2. Gå til den disk på proxmox hvor i gemmer ISO images
3. Klik på `Download from URL` og indsæt kali .iso linket f.eks `https://cdimage.kali.org/kali-2023.4/kali-linux-2023.4-installer-amd64.iso`
 ![download from url](../images/proxmox_dl_url.png)
4. Vent på at iso filen er hentet 
5. Lav en ny vm i proxmox  
 ![proxmox ny vm](../images/opnsense_proxmox_vm_1.png)
6. konfigurer den nye vm med følgende settings  
![ny vm konfig](../images/proxmox_kali_vm_settings.png)
7. Vent på at installationen afsluttes
8. Start jeres kali vm og log på med user:pass `kali:kali` 
9. Åbn en browser og gå til opnsense webinterfacet på 192.168.1.1 (fortsæt på trods af certifikat advarsel)
10. Åbn en cli prompt og kontroller din ip adresse med `ip a` 
11. Aftal i gruppen hvordan i kan anvende den samme kali vm. Måske kan i lave flere brugere på kali?

## Links

- Offsec's egen guide: [https://www.kali.org/docs/virtualization/install-proxmox-guest-vm/](https://www.kali.org/docs/virtualization/install-proxmox-guest-vm/)
