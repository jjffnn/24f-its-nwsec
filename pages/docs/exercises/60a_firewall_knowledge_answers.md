---
hide:
  - footer
---

# Øvelse 50 - Netværkssegmentering

## Information

Øvelsen er en gruppeøvelse.

Firewalls er afgørende sikkerhedsenheder, der anvendes til at filtrere og kontrollere trafik mellem et internt netværk og det eksterne internet eller mellem forskellige segmenter af et netværk.  
Der findes forskellige typer af firewalls, og de kan filtrere trafik på forskellige måder.
I denne øvelse skal i undersøge forskellige typer af firewalls, hvad de er i stand til og hvilke svagheder de har.

## Instruktioner

1. I jeres gruppe skal i undersøge forskellige firewall typer og hvad de kan. De forskellige typer kan være: 
    - Pakkefiltreringsfirewall
    - Stateful Inspection Firewall
    - Application Layer Firewall (Proxy Firewalls)
    - Next-Generation Firewall (NGFW)
    - Proxy Server Firewall (f.eks Web Application Firewall)
2. Lav en kort beskrivelse af hvad de forskellige typer kan, hvilket lag i OSI modellen de arbejder på og hvad deres svagheder er (hvordan de kan kompromitteres ved f.eks ip spoofing) 
3. Dokumenter jeres undersøgelse i jeres gruppes gitlab projekt.

Firewalls er afgørende sikkerhedsenheder, der anvendes til at filtrere og kontrollere trafik mellem et internt netværk og det eksterne internet eller mellem forskellige segmenter af et netværk. Der findes forskellige typer af firewalls, og de kan filtrere trafik på forskellige måder. Her er nogle almindelige typer af firewalls og hvad de er i stand til at filtrere:

- Pakkefiltreringsfirewall:
    - Filtrering: Denne type firewall arbejder på netværkslaget (lag 3) og inspicerer individuelle netværkspakker. Den tager beslutninger baseret på informationen i IP-headeren, såsom kilde- og destinations-IP-adresser samt portnumre.
    - Evne til at filtrere: Den kan blokere eller tillade trafik baseret på specifikke IP-adresser, portnumre og protokoller, men har ikke indgående kendskab til tilstanden af forbindelsen- 
- Stateful Inspection Firewalls:
    - Filtrering: Arbejder også på netværkslaget, men har en dybere forståelse af tilstanden af en forbindelse. Den holder styr på tilstanden af aktive forbindelser og træffer beslutninger baseret på tilstandsoplysninger.
    - Evne til at filtrere: Kan tage mere informerede beslutninger ved at analysere tilstandsoplysninger, hvilket gør dem mere effektive til at håndtere komplekse protokoller og forbindelser- 
- Application Layer Firewalls (Proxy Firewalls):
    - Filtrering: Arbejder på applikationslaget (lag 7) og inspicerer data på applikationsniveau. Proxy-firewalls fungerer som mellemled mellem brugerne og internettet ved at håndtere trafik på vegne af brugerne.
    - Evne til at filtrere: Kan filtrere baseret på specifikke applikationsprotokoller og kan give mere detaljeret kontrol over applikationslagstrafik- 
- Next-Generation Firewalls (NGFW):
    - Filtrering: Kombinerer funktionerne i traditionelle stateful inspection-firewalls med yderligere funktioner som intrusion detection/prevention, antivirus og dyb pakkeinspektion.
    - Evne til at filtrere: Er i stand til at analysere indholdet af datapakker for at identificere skadelige indhold og applikationsprotokoller, hvilket giver mere avanceret sikkerhed- 
- Proxy Server Firewalls:
    - Filtrering: Fungerer som en mellemmand mellem interne brugere og internettet ved at håndtere webanmodninger på vegne af brugerne.
    - Evne til at filtrere: Kan filtrere indholdet af webtrafik, blokere adgang til bestemte websteder og give detaljeret kontrol over webbrowsing- 
- Deep Packet Inspection (DPI):
    - Filtrering: Anvender avanceret analyse af datapakker, hvor både header- og indholdsoplysninger inspiceres.
    - Evne til at filtrere: Giver detaljeret kontrol ved at analysere indholdet af datapakker, hvilket muliggør identifikation og blokering af specifikke applikationer og protokoller.

## Links

- [Netwrix - [...]Common Types of Network Security Devices[...]](https://blog.netwrix.com/2019/01/22/network-security-devices-you-need-to-know-about/)
- [techtarget - 5 different types of firewalls explained](https://www.techtarget.com/searchsecurity/feature/The-five-different-types-of-firewalls)