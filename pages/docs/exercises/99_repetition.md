---
hide:
  - footer
---

# Øvelse 99 - Repetition af faget

### Information

Som forberedelse til eksamen i faget er det en god ide at opsummere hvilke emner og læringsmål der er arbejdet med gennem semestret.  
Det vil være individuelt hvor meget der skal læses op før eksamen og derfor er denne Øvelse en individuel Øvelse.

### Instruktioner

1. Lav et dokument som du bruger til at få et overblik over fagets læringsmål, uge for uge.  
For hver uge laver du en checkliste over de læringsmål der gælder for ugen samt en checkliste for de Øvelser der er stillet i ugen.  

Foreslået struktur:

```md 
# Uge xx - xxxxx
    - Læringsmål:
        - [ ] læringsmål 1: 
        - [ ] læringsmål 2: 
        - [ ] læringsmål n:
    - Øvelser:
        - [ ] Øvelse 1: 
        - [ ] Øvelse 2: 
        - [ ] Øvelse n:
# Uge yy - yyyyy 
    - Læringsmål:
        - [ ] læringsmål 1: 
        - [ ] læringsmål 2: 
        - [ ] læringsmål n:
    - Øvelser:
        - [ ] Øvelse 1: 
        - [ ] Øvelse 2: 
        - [ ] Øvelse n:
(indsæt resten af ugerne herunder)
```
2. Når du har en liste for alle uger i faget skal du vurdere hvor du selv er ifht. læringsmål og Øvelser.  
Her krydser du de læringsmål og Øvelser af som du mener du har styr på, det vil sige du ender med et overblik over læringsmål og Øvelser som du skal genbesøge i din repetition.  
3. Afsæt den nødvendige tid og lav Øvelser + genbesøg materialer, så du opnår viden, færdigheder og kompetencer i overenstemmelse med fagets læringsmål.  
