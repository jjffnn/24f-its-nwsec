---
hide:
  - footer
---

# Øvelse 21 - OPNsense på proxmox

## Information

OPNsense er open source netværks software der i sin grund funktionalitet er en router. 
Udover routing funktionalitet findes firewall, nextgen firewall, IDS/IPS, netflow monitorering og meget mere.  

Du kan læse mere på [opnsense.org](https://opnsense.org/)  
En anden vigtig ressource er OPNsense dokumentationen som du finder på [docs.opnsense.org](https://docs.opnsense.org/)

I netværks- og kommunikationsikkerhed skal i bruge OPNsense på jeres proxmox installation og øvelsen er derfor en gruppe øvelse.

Denne øvelse guider jer igennem basis opsætning af OPNsense som router på proxmox.

## Instruktioner

**Lav en ny network bridge i proxmox** 

1. Lav en ny Linux bridge 
 ![ny linux bridge](../images/opnsense_proxmox_vm_4.png)
2. Giv den et navn f.eks vmbr10
 ![Alt text](../images/opnsense_proxmox_vm_5.png)
3. Klik `Apply configuration` for at aktivere den nye bridge

**Download OPNsense DVD image (iso fil)**

1. Hent nyeste opnsense image til din egen computer fra [https://opnsense.org/download/](https://opnsense.org/download/) du skal vælge **DVD** som *image type*
![OPNsense iso download](../images/opnsense_proxmox_iso.png)
2. Udpak den hentede fil, som er komprimeret med bzip2, med f.eks [7zip](https://www.7-zip.org/download.html). Du skal ende med at have en `.iso` fil f.eks `OPNsense-23.7-dvd-amd64.iso`  

**Importer ISO til proxmox**

1. Log ind på proxmox
2. Gå til den disk på proxmox hvor du gemmer ISO images
3. Klik på "upload", vælg .iso filen og vent på at den importeres
 ![Proxmox iso upload](../images/opnsense_proxmox_iso_dl.png)

**Lav en vm med proxmox iso**

 1. Lav en ny vm i proxmox  
 ![proxmox ny vm](../images/opnsense_proxmox_vm_1.png)
 2. Konfigurer jeres vm som nedenstående
 ![vm config](../images/opnsense_proxmox_vm_2.png)
3. Tilføj endnu et netkort til jeres vm
 ![proxmox ekstra netkort 1](../images/opnsense_proxmox_vm_3.png)
 ![proxmox ekstra netkort 2](../images/opnsense_proxmox_vm_3.1.png)
4. Start jeres vm og tryk på `console`, vent på at opnsense starter
5. Log ind som `installer` password `opnsense` for at starte installationen
 ![installer login](../images/opnsense_proxmox_vm_6.png)
6. vælg dansk tastatur  
 ![dk keyboard](../images/opnsense_proxmox_vm_7.png)
7. Vælg filsystem  
 ![opnsense filsystem 1](../images/opnsense_proxmox_vm_8.png)
 ![opnsense filsystem 2](../images/opnsense_proxmox_vm_9.png)
 ![opnsense filsystem 3](../images/opnsense_proxmox_vm_10.png)
 ![opnsense filsystem 4](../images/opnsense_proxmox_vm_11.png)
8. Complete og reboot  
 ![Complete og reboot](../images/opnsense_proxmox_vm_12.png)
9. sluk for jeres opnsense vm  
 ![sluk opnsense vm](../images/opnsense_proxmox_vm_13.png)
10. dobbeltklik på CD/DVD drive og fjern image filen fra drevet (do not use any media)  
 ![eject image fra dvd drive](../images/opnsense_proxmox_vm_14.png)
11. start opnsense vm, der bootes nu fra harddisken istedet for dvd drevet  
12. Login med `root` og pw `opnsense`
13. Vælg assign interfaces, nej til LAGGs og VLANs  
 ![assign interfaces 1](../images/opnsense_proxmox_vm_15.png)
14. Assign WAN til vtnet0 og LAN til vtnet1, bekræft med `y` og vent på prompt  
 ![assign interfaces 2](../images/opnsense_proxmox_vm_17.png)
15. Bekræft at WAN har en adresse på 10.56.18 netværket og LAN har 192.168.1.1.    
Hvis i vil ændre LAN adressen kan i gøre det ved at vælge `2) Set interface IP address`. Her kan i også vælge om DHCP skal være aktiveret på LAN netværket.  
16. I bør nu kunne tilgå opnsense webinterfacet fra en browser på en proxmox vm tilsluttet vmbr10 på LAN adressen (192.168.1.1).  
17. Aftal i gruppen hvordan i kan anvende den samme opnsense vm. Måske kan i lave flere brugere på opnsense?

## Links

- Video tutorial (nogenlunde som ovenstående) [Running OPNSense Firewall and Router in a Proxmox VM](https://youtu.be/krMMb-6G9JM?si=XXCw1B4wEZj2_kDt)
- OPNsense installation get started [https://opnsense.org/users/get-started/](https://opnsense.org/users/get-started/)
- OPNsense image download [https://opnsense.org/download/](https://opnsense.org/download/)