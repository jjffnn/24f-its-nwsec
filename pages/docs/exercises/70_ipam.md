---
hide:
  - footer
---

# Øvelse 70 - Dokumentation og IPAM 

## Information

Dette er en gruppeøvelse

Kortlægning af virksomhedens it-systemer, tjenester, enheder osv. er ofte der, hvor sikkerhedsarbejdet starter. 
Det er fordi det er nødvendigt at vide, hvad vi har for senere at kunne afgøre om og, hvordan vi kan beskytte det.

Virksomheder der gerne vil d-mærke certificeres bruger digital trygheds skabelon til at kortlægge hvad de har.  
Skabelonen giver et overblik over virksomheden men er ikke en detaljeret oversigt over hvordan netværket er konfigureret. 

Inden du fortsætter så hent skabelonen og se hvad den kan i forhold til at kortlægge netværksudstyr? 
Skabelonen kan finde her: [Template til generel kortlægning](https://d-maerket.dk/wp-content/uploads/2022/05/Template-til-generel-kortlaegning-kriterie-1-.xlsx)

Netværk kan ofte blive store og derfor er det nødvendigt at holde styr på hvordan det er indrettet med ip adresser. Små netværk kan udemærket dokumenteres med netværksdiagrammer, men i større løsninger er det nødvendigt at have en IPAM (IP Address Management) løsning.
IPAM står for "IP Address Management," hvilket på dansk betyder "styring af IP-adresser."  
IPAM refererer til praksissen med at planlægge, administrere og overvåge IP-adresser i et netværk.

Link til [Netbox VM (.ova fil)](https://drive.google.com/file/d/1ZfPr_P2llgnJZsvLj50ZHaIAsgH8b1AA/view?usp=sharing) (er også på itslearning)

## Instruktioner

1. Læs om IPAM og snak i jeres gruppe om hvad det er og hvad det bruges til, brug links herunder til at undersøge IPAM: 
    - [Bluecat - what-is-ipam](https://bluecatnetworks.com/glossary/what-is-ipam/)
    - [OpUtils - What is IPAM?](https://www.manageengine.com/products/oputils/what-is-ipam.html)
2. papir+blyant øvelse.  
    Lav en IP subnet oversigt over devices i har derhjemme og de tilhørende interne og eksterne netværk.  
3. Research hvad netbox er, lav en liste over hvad det kan og hvad det ikke kan.
4. Set netbox VM op i vmware workstation og log ind på web interfacet. 
Brug [getting started](https://docs.netbox.dev/en/stable/getting-started/planning/) fra netbox dokumentationen (som er lavet med mkdocs!!)  
5. Dokumentér et valgfrit netværk via netbox web interfacet. Søg på nettet efter *large network diagram* eller *large network topology* f.eks. [CISCO-example.png](https://www.conceptdraw.com/How-To-Guide/picture/CISCO-example.png)

    ```
    Netbox credentials:  
    consolen viser ip adressen
    netbox:netbox som password til web interface.
    andre logins i console
    ```   
6.  Diskuter i gruppen hvad i synes er en god måde at holde styr på et større netværk med perspektiv til hvordan i kan gøre det i eksamensprojektet?  
Research evt. hvilke andre måder i kan lave IPAM dokumentation på i eksamensprojektet.


## Links

- [netbox dokumentation](https://netbox.readthedocs.io/en/stable/)

