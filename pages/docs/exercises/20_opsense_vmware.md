---
hide:
  - footer
---

# Øvelse 20 - OPNsense på vmware workstation

## Information

OPNsense er open source netværks software der i sin grund funktionalitet er en router. 
Udover routing funktionalitet findes firewall, nextgen firewall, IDS/IPS, netflow monitorering og meget mere.  

Du kan læse mere på [opnsense.org](https://opnsense.org/)  
En anden vigtig ressource er OPNsense dokumentationen som du finder på [docs.opnsense.org](https://docs.opnsense.org/)

I netværks- og kommunikationsikkerhed skal du bruge OPNsense rigtig meget, både i vmware workstation og på din gruppes proxmox installation.
Denne øvelse guider dig igennem basis opsætning af OPNsense som router i vmware workstation.

Instruktionerne herunder udføres alle i vmware workstation pro 17.
Øvelsen er individuel.  

## Instruktioner

**Konfigurer vmnet8 i vmware workstation** 

1. Åbn `Edit->Virtual network editor`
2. Bekræft at netværket `vmnet8` er konfigureret som nedenstående:  
 ![vmnet8 settings](../images/opnsense_vmware_vm_1.png)

**Download OPNsense DVD image (iso fil)**

1. Hent nyeste opnsense image til din egen computer fra [https://opnsense.org/download/](https://opnsense.org/download/) du skal vælge **DVD** som *image type*
![OPNsense iso download](../images/opnsense_proxmox_iso.png)
2. Udpak den hentede fil, som er komprimeret med bzip2, med f.eks [7zip](https://www.7-zip.org/download.html).  
Du skal ende med at have en `.iso` fil f.eks `OPNsense-23.7-dvd-amd64.iso`  

**Konfigurer virtuel maskine i vmware workstation**

1. Lav en ny virtuel maskine   
 ![new vm](../images/opnsense_vmware_vm_2.png)
2. Indstil som nedenstående  
 ![new vm 2](../images/opnsense_vmware_vm_3.png)  
 ![new vm 3](../images/opnsense_vmware_vm_4.png)  
 ![new vm 4](../images/opnsense_vmware_vm_5.png)  
 ![new vm 5](../images/opnsense_vmware_vm_6.png) 

3. Klik på `Customize Hardware` og tilføj en `Network Adapter`  
 ![new vm 6](../images/opnsense_vmware_vm_7.png)  
 ![new vm 7](../images/opnsense_vmware_vm_8.png) 
4. Sæt `Memory` til 2GB  
 ![new vm 8](../images/opnsense_vmware_vm_9.png)  
5. Konfigurer netværkskortene til hhv. VMnet2 og VMnet8  
 ![new vm 9](../images/opnsense_vmware_vm_10.png)  
6. klik `Close` og derefter `Finish` for at starte installationen  

**Konfigurer opnsense**

Når opnsense er klar vises en oversigt  

1. Log ind som `installer` password `opnsense` for at starte installationen  
 ![new vm 10](../images/opnsense_vmware_vm_11.png) 
2. vælg dansk tastatur  
 ![dk keyboard](../images/opnsense_proxmox_vm_7.png)
3. Vælg filsystem og vent derefter på at systemet klones  
 ![opnsense filsystem](../images/opnsense_proxmox_vm_8.png)
 ![new vm 10](../images/opnsense_proxmox_vm_9.png)
 ![new vm 11](../images/opnsense_vmware_vm_12.png)
 ![new vm 12](../images/opnsense_proxmox_vm_11.png)
4. Complete og reboot (det anbefales ikke at ændre password i denne øvelse, det bør dog gøres i produktion!)  
 ![new vm 13](../images/opnsense_proxmox_vm_12.png)
5. sluk for din opnsense vm  
 ![new vm 14](../images/opnsense_vmware_vm_13.png)
6. Deaktiver CD/DVD drevet  
 ![new vm 15](../images/opnsense_vmware_vm_14.png)
7. start opnsense vm, der bootes nu fra harddisken istedet for dvd drevet  
8. Kontroller at du kan tilgå opnsense webinterfacet fra en browser på en anden vm (f.eks fra Kali) tilsluttet vmnet2 (LAN) på LAN adressen 192.168.1.1.  
9. Naviger rundt i webinterfacet for at udforske hvad opnsense byder på.  
    - Er der noget du kan genkende? 
    - Er der noget du ikke har set før? 
    - Er der noget du kender, men ikke har arbejdet med før?

## Links

- OPNsense installation get started [https://opnsense.org/users/get-started/](https://opnsense.org/users/get-started/)
- OPNsense image download [https://opnsense.org/download/](https://opnsense.org/download/)