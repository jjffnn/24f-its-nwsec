---
hide:
  - footer
---

# Øvelse 81 - Docker og portainer host 

## Information

- [Installer Portainer](https://docs.portainer.io/start/install-ce/server/docker/linux)
- [Install graylog portainer](https://jalokin.com/tech_stuff/graylog_install/)

```yaml
# From https://github.com/Graylog2/docker-compose/tree/main/open-core

version: "3.8"

services:
  mongodb:
    image: mongo:5.0
    volumes:
      - "mongodb_data:/data/db"
    restart: unless-stopped

  opensearch:
    image: opensearchproject/opensearch:2.4.0
    environment:
      - "OPENSEARCH_JAVA_OPTS=-Xms1g -Xmx1g"
      - "bootstrap.memory_lock=true"
      - "discovery.type=single-node"
      - "action.auto_create_index=false"
      - "plugins.security.ssl.http.enabled=false"
      - "plugins.security.disabled=true"
    ulimits:
      memlock:
        hard: -1
        soft: -1
      nofile:
        soft: 65536
        hard: 65536
    volumes:
      - "os_data:/usr/share/opensearch/data"
    restart: unless-stopped

  graylog:
    hostname: "server"
    image: ${GRAYLOG_IMAGE:-graylog/graylog:5.1.5}
    depends_on:
      opensearch:
        condition: "service_started"
      mongodb:
        condition: "service_started"
    entrypoint: "/usr/bin/tini -- wait-for-it opensearch:9200 --  /docker-entrypoint.sh"
    environment:
      GRAYLOG_NODE_ID_FILE: "/usr/share/graylog/data/config/node-id"
      GRAYLOG_PASSWORD_SECRET: "${GRAYLOG_PASSWORD_SECRET}"
      GRAYLOG_ROOT_PASSWORD_SHA2: "${GRAYLOG_ROOT_PASSWORD_SHA2}"
      GRAYLOG_HTTP_BIND_ADDRESS: "0.0.0.0:9100"
      GRAYLOG_HTTP_EXTERNAL_URI: "http://localhost:9100/"
      GRAYLOG_ELASTICSEARCH_HOSTS: "http://opensearch:9200"
      GRAYLOG_MONGODB_URI: "mongodb://mongodb:27017/graylog"
    ports:
    - "2055:2055/udp"   # Netflow udp
    - "5044:5044/tcp"   # Beats
    - "514:5140/udp"   # Syslog
    - "5140:5140/tcp"   # Syslog
    - "5555:5555/tcp"   # RAW TCP
    - "5555:5555/udp"   # RAW TCP
    - "9100:9100/tcp"   # Server API
    - "12201:12201/tcp" # GELF TCP
    - "12201:12201/udp" # GELF UDP
    #- "10000:10000/udp" # Custom UDP port
    - "13301:13301/tcp" # Forwarder data
    - "13302:13302/tcp" # Forwarder config
    volumes:
      - "graylog_data:/usr/share/graylog/data/data"
      - "graylog_journal:/usr/share/graylog/data/journal"
      - "graylog_config:/usr/share/graylog/data/config"
    restart: unless-stopped

volumes:
  mongodb_data:
  os_data:
  graylog_data:
  graylog_journal:
  graylog_config:
```

![](../images/graylog_portainer_vars.png)

[opnsense syslog parsers](https://github.com/IRQ10/Graylog-OPNsense_Extractors)

## Ressourcer



