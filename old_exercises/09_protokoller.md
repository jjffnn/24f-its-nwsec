---
hide:
  - footer
---

# Øvelse 9 - Protokoller

## Information

Protokoller er udbredte indenfor netværk, nogle af dem implementerer sikkerhed, nogle af dem gør ikke.  
Formålet med denne Øvelse er at træne brugen af wireshark til at genkende protokoller.  

## Instruktioner

1. Find to-tre protokoller fra wiresharks test pakker som du kan genkende 
2. Forstå hvad der foregår, evt. vha. google
3. Tag noter, og vis det næste gang

## Links

- [wirehark intro](https://www.varonis.com/blog/how-to-use-wireshark)
- [wireshark test pakker](https://wiki.wireshark.org/SampleCaptures)
- [tcpdump BPF examples](https://hackertarget.com/tcpdump-examples/)
