---
hide:
  - footer
---

# Øvelse 5 - internet speeds

## Information

Der er flere services på nettet der tilbyder at teste båndbredde

## Instruktioner

1. Gå på [speedtest](https://speedtest.net)
2. Hvad er hastigheden?
3. Best guess: hvad er bottleneck, og hvorfor.

Prøv evt. at køre den samtidigt på to maskiner på samme subnet/wifi.

## Links

- none supplied