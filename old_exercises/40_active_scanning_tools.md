---
hide:
  - footer
---

# Øvelse 40 - Aktive skanningsværktøjer

## Information

Netværk og services er som regel ikke en statisk størrelse og derfor er det en god ide jævnligt at afdække hvordan netværket ser ud.  
Der findes mange værktøjer til at afdække det, vi har tidligere kigget på passive værktøjer og skal nu kigge på et par aktive skanningsværktøjer.  

Husk at trusselsaktører bruger både passiv og aktiv skanning når de afdækker angrebsfladen og undersøger hvilke attack vectors de kan benytte.  
De leder efter ting der er eksponeret og giver hints til om komponenter og services er sårbare og kan udnyttes. 

Øvelsen tager udgangspunkt i en virtuel maskine fra [vulnhub](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) som har konfigureret en del sårbare services.  
Det gør det muligt at bruge aktive *interne* skanningsværktøjer for at få indsigt i hvad de kan og hvilket output de giver.  
Husk at perspektivere til store netværk med mange maskiner og services!

Der findes også *eksterne* værktøjer som i skal afprøve, men ikke på den sårbare virtuelle maskine (der forhåbentlig ikke er eksponeret mod internettet...)

Formålet med Øvelsen er at få et indblik i hvad der kan opdages med aktiv skanning med henblik på at kunne detektere og mitigere eventuelle sårbarheder.

Øvelsen skal laves i teams hvor i arbejder individuelt og i fællesskab snakker om det i finder ud af. I skal også rapportere i et fælles dokument.  

Her er et overblik over værktøjer til aktiv scanning/overvågning

**interne**  

- [NMAP](https://nmap.org/) [tutorial](https://phoenixnap.com/kb/nmap-commands)
- [sslscan](https://www.kali.org/tools/sslscan/)
- [openvas](https://www.openvas.org/)
- [nessus](https://www.tenable.com/products/nessus)


**eksterne**

- [shodan](https://www.shodan.io/) (søgninger, network monitoring)
- [whois](https://mxtoolbox.com/SuperTool.aspx?action=whois%3aucl.dk&run=toolpage) Hvad viser den grønne knap med "find problems"
- [ssllabs](https://www.ssllabs.com/ssltest/) [test af ucl.dk](https://www.ssllabs.com/ssltest/analyze.html?d=ucl.dk&latest) 
- [security trails](https://www.recordedfuture.com/platform/attack-surface-intelligence) Attack Surface Intelligence


## Instruktioner

**Del 1: Interne værtøjer til aktiv skanning**

1. Hent [Vulnerable Pentesting Lab Environment: 1](https://www.vulnhub.com/entry/vulnerable-pentesting-lab-environment-1,737/) ([mirror](https://drive.google.com/drive/folders/1gPSNA-2OF9dDJTdhUOcn2N3zPjRBUSWm?usp=sharing))
2. Lav et dokument til at rapportere det i finder undervejs.
3. Åbn maskinen i *vmware workstation* og konfigurer netværk. Maskinen henter sin IP fra DHCP så det vmnet du bruger skal være konfigureret med DHCP i *network manager*. Netværket skal ikke have internetadgang.
4. Sæt en kali maskine på samme vmnet.
5. Tegn et netværksdiagram med hvad du ved på nuværende tidspunkt.
6. Gør følgende med NMAP fra kali maskinen, du skal have wireshark kørende på samme tid:
    1. scan netværket med ping scan, gem scanning med `-oA <filename>`, hvad finder du og hvordan ser det ud i wireshark? 
    2. Gem en pcapng fil med trafikken fra wireshark
    3. Scan den sårbare maskine, du burde finde mindst 5 åbne porte.
    4. Hvilke services og versioner er på de 5 porte?
    5. Er der nogle services der er sårbare og hvor kan det undersøges? (hint! cve databaser og exploits) 
7. Gør følgende med sslscan fra kali maskinen:
    1. prøv ssl scan mod nogen af de services der kører på vple ie. `sslscan <vmip:port>` 
    2. Hvad er output fra sslscan? 
    3. er der services der bruger TLS ?
8. [gobuster](https://www.kali.org/tools/gobuster/#tool-documentation)/[dirbuster](https://www.kali.org/tools/dirbuster/)/[ffuf](https://www.kali.org/tools/ffuf/)/[wfuzz](https://www.kali.org/tools/wfuzz/) er værktøjer til at "fuzze" endpoints på services med henblik på at afdække hvilke endpoints der findes på en service.  
(måske skal du installere seclists [https://www.kali.org/tools/seclists/](https://www.kali.org/tools/seclists/))  
Afprøv de 4 værktøjer:  
    1. Brug wireshark når du fuzzer og gem relevant (http) trafik fra de forskellige værktøjer som en `.pcapng` fil
    2. Prøv gobuster med:  
    `gobuster dir --url http://<vmip>:1336 -w /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt | tee gobuster1336.txt`  
    og se hvad du finder ?
    3. prøv dirbuster med:  
    `dirb http://<vmip>:1336 /usr/share/wordlists/seclists/Discovery/Web-Content/raft-small-words.txt | tee dirbuster1336.txt`  
    og se hvad du finder ?
    4. prøv ffuf med:  
    `ffuf -w /usr/share/seclists/Discovery/Web-Content/raft-small-words.txt -u http://<vmip>:1336/FUZZ -c -mc all -r  -fw 24 | tee ffuf1336.txt`  
    og se hvad du finder [ffuf cheatsheet](https://cheatsheet.haax.fr/web-pentest/tools/ffuf/)
    5. prøv wfuzz med:  
    `wfuzz -w /usr/share/seclists/Discovery/Web-Content/raft-small-words.txt -u http://<vmip>:1336/FUZZ -c -R 5 --hc 404 | tee wfuzz1336.txt`  
    og se hvad du finder
    6. Sammenlign resultaterne i de 4 filer du har lavet: 
        1. hvilket værktøj fandt mest ?
        2. hvilket værktøj tog længst tid ?
9. Hvilke services synes i er acceptable at have kørende og hvilke er der ikke nogen grund til at eksponere? Nogle specifikke porte?
10. Hvad vil i gøre for at "lukke" de uacceptable services?
11. Er aktiv skanning nok til at finde sårbarheder? Kan der gøres yderligere for at sikre netværk og services?

**Del 2: Eksterne værtøjer til aktiv skanning**

1. Shodan
    1. Lav en konto på shodan, den der er gratis (tip! hold øje på black friday hvis du vil have rabat til at blive member)
    2. Læs om hvad shodan kan ifht. netværks sikkerhed: [https://help.shodan.io/the-basics/what-is-shodan](https://help.shodan.io/the-basics/what-is-shodan)
    3. Læs om [shodan søgninger](https://help.shodan.io/the-basics/search-query-fundamentals)
    4. Lav følgende søgninger
        - `airplay port:5353`
        - `has_screenshot:true`
        - `http.title:"Nordex Control" "Windows 2000 5.0 x86" "Jetty/3.1 (JSP 1.1; Servlet 2.2; java 1.6.0_14)"`
    5. Udforsk selv yderligere muligheder, kig f.eks i de søgninger andre har lavet:  
    [https://www.shodan.io/explore/recent](https://www.shodan.io/explore/recent)
2. Whois
    1. Hvad viser den grønne knap med "find problems" på [whois](https://mxtoolbox.com/SuperTool.aspx?action=whois%3aucl.dk&run=toolpage) ?
3. ssllabs
    1. Hvad kan i finde ud af på [ssllabs](https://www.ssllabs.com/ssltest/) 
    2. [test ucl.dk](https://www.ssllabs.com/ssltest/analyze.html?d=ucl.dk&latest) og se om der er problemer

## Links

- Inspiration [ippsec interface](https://youtu.be/yM914q6zS-U)
- Introduction to [shodan monitor](https://youtu.be/T-9UvZ-l-tE)