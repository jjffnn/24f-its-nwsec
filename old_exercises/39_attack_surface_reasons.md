---
hide:
  - footer
---

# Øvelse 39 - Huller i suppen

## Information

Umiddelbart kan det virke mærkeligt at det, den dag i dag, stadig er muligt for trusselsaktører at få adgang til systemer men faktum er at det sker ofte.  
Hvad er årsagen til det? Og hvorfor "fikser" organisationer ikke bare deres systemer så de er "sikre" ?

I denne Øvelse skal i undersøge årsagerne til at systemer er usikre og sårbare. Links er til inspiration, find gerne flere selv hvis i synes det er nødvendigt. 

## Instruktioner

Besvar følgende:

1. Giv mindst 5 *typiske* eksempler på hvorfor systemer kan have sårbarheder, begrund dem sagligt med kildehenvisninger.
2. Skriv et bud på en procedure der kan bruges til at afdække attack surface.

## Links

- [Mitre active scanning](https://attack.mitre.org/techniques/T1595/)
- [CIS18](https://www.cisecurity.org/controls/cis-controls-navigator/) control 1: inventory, eller control 7: Continuous Vulnerability Management
- [OWASP attack surface analysis](https://cheatsheetseries.owasp.org/cheatsheets/Attack_Surface_Analysis_Cheat_Sheet.html) Skrevet til applikationer men god inspiration
