---
hide:
  - footer
---

# Øvelse 31 - Let's encrypt

## Information

I denne Øvelse skal vil vi sætte en webserver op med et valid certifikat fra let's encrypt. Vi bruger digital ocean til det.

I Øvelsebeskrivelsen bliver der brugt Ubuntu 20.04. Man kan vælge at bruge 22.04 i stedet. Desuden bliver der brugt `sudo`. Dette er ikke nødvendigt hvis man er logget ind som root.

Til denne Øvelse skal der bruges et digital ocean login og et domænenavn.

## Instruktioner

0. Lav en [droplet](https://docs.digitalocean.com/products/droplets/how-to/create/#create-a-droplet-in-the-control-panel)

  Dette er en cloud løsning, så man skal huske at slukke for sin virtuelle maskine npr man er færdig. Prisen er pt. $4/måned.

  Hvis man ikke kan lide at logge ind vha. ssh, så er der mulighed for at bruge "console" fra digital oceans web interface

1. Installer en webserver. Følg [denne](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04) guide.

  Det er ikke nødvendigt med "Server block" delen

2. Configurer et certifikat vha. let's encrypt. Følg [denne](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-20-04) guide.

3. Check at det virker ved at gå ind på hjemmesiden, og se på certifikatet

## Links

* [Om let's encrypt](https://letsencrypt.org/da/how-it-works/#:~:text=Form%C3%A5let%20med%20Let's%20Encrypt%20og,certifikat%20management%20agent%20p%C3%A5%20webserveren.)
