---
hide:
  - footer
---

# Øvelse 12 - Rekognosering 

## Information

Rekognosering bruges af både trusselsaktører og sikkerhedsprofessionelle.  
Rekognosering giver overblik over hvilke tekniske foranstaltninger en virksomhed har.  
Den information kan så bruges eller misbruges.

Formålet med Øvelsen er at du lærer forskellige værktøjer til rekognosering og især værktøjer der bruger DNS opslag.  

Værktøjer du skal undersøge er:  

- whois
- nslookup
- dig
- DNSdumpster
- shodan
- Securitytrails

## Instruktioner

1. Gennemfør THM rummet: [[https://tryhackme.com/room/passiverecon](https://tryhackme.com/room/passiverecon)]([https://tryhackme.com/room/passiverecon](https://tryhackme.com/room/passiverecon))
2. Læs om og registrer en konto på securitytrails.com
1. Beskriv forskellen på aktiv og passiv rekognoscering
2. Lav en liste med de værktøjer du kender til aktiv rekognoscering, beskriv kort deres formål
3. Lav en liste med de værktøjer du kender til passiv rekognoscering, beskriv kort deres formål
4. Beskriv med dine egne ord hvad en fjendtlig aktør kan bruge rekognoscering til
5. Beskriv med dine egne ord hvad du kan bruge rekognoscering til, når du skal arbejde med sikkerhed
6. Brug kun PASSIV rekognoscering til at undersøge ucl.dk. Lav en lille rappport med de ting du har fundet ud af og hvordan en angriber evt. kan misbruge oplysningerne. Rapporten skal du IKKE offentliggøres på gitlab, kun på din egen computer, resultater kan du fortælle om til næste undervisning!

## Links

- threath intelligence tools [https://www.farsightsecurity.com/](https://www.farsightsecurity.com/)