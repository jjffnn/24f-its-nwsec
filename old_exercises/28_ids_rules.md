---
hide:
  - footer
---

# Øvelse 28 - IDS/IPS Regler

## Information

Suricata bruger regler til at detektere signaturer i netværkstrafik. 

Regler er opbygget af 3 dele: 

- **action** definerer hvad der skal ske når der er et match, f.eks alert eller drop
- **header** definerer retning, ip adresser/netværk, protokol og port
- **rule options** definerer yderligere konfiguration af en regel

Du har allerede lavet din første regel i Øvelse 27 - Suricata opsætning hvor du tilføjede denne regel til local.rules filen:  
`alert icmp any any -> any any (msg:"ping/icmp detected"; sid:5000000;)`


Her er en forklaring af hvordan reglen er opbygget:

- `alert` er **action**
- `icmp any any -> any any` er **header** `icmp` er protokollen, `any any -> any any` betyder fra alle ip adresser på alle porte til alle ip adresser på alle porte. Her kunne være specificeret en specifik ip adresse/netværk og port i både afsender og modtager. Retningen `->` kunne have været specificeret i begge retninger `<>`.
- `(msg:"ping/icmp detected"; sid:5000000;)` er **rule options**, `msg` er den tekst der skrives i fast.log og eve.json. `sid` **skal** angives **og** være unik for hver regel.

Det kan være en fordel at have pcap filen åben i wireshark samtidig med at du laver Øvelsen.  

## Instruktioner

1. Læs om regler i [Suricata dokumentationen](https://suricata.readthedocs.io/en/suricata-6.0.11/rules) og dan dig et overblik over mulighederne.  
2. Lav en ny mappe på den vm hvor du har installeret Suricata
3. På suricata maskinen hent filen [2018_07_15.zip](../../pcap/2018_07_15.zip) og udpak pcap filen fra `2018-07-15-traffic-analysis-exercise.pcap.zip` (brug wget, **zip password er _infected_**)
4. Skriv en regel i `/etc/suricata/rules/local.rules` der laver en alert på udp trafik fra `10.0.0.201 port 63448` til `5.138.53.160 port 41230`  
Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?
5. Skriv en regel der alerter på **http** trafik fra alle ip og porte til alle ip og porte hvor http content type er **image/jpeg**  
Dokumenter hvor mange alerts du får og hvad output fra de relevante alerts er i fast.log ?
6. Skriv en regel der alerter på **http** trafik fra alle ip og porte til alle ip og porte hvor http content type er **application/x-bittorrent**  
7. Skriv en regel der alerter på **http** trafik fra alle ip og porte til alle ip og porte hvor ordet **Betty_Boop** indgår.  
Dokumenter hvor mange alerts du ser, samt hvilke ip adresser og porte der er involveret i kommunikationen ?

## Links

- [Suricata rules](https://suricata.readthedocs.io/en/suricata-6.0.11/rules/index.html)
- [Snort rules](http://manual-snort-org.s3-website-us-east-1.amazonaws.com/node27.html)
- [Malware-traffic-analysis.net](https://www.malware-traffic-analysis.net/)
- [Video: Writing Suricata Rules](https://youtu.be/d-t-8lJ2PII)