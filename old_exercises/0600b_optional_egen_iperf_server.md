---
hide:
  - footer
---

# Øvelse 6b - **Optional** din egen iperf server

## Information

iperf er et open source client/server system, så man kan sætte sin egen server og lave sine egne tests.

## Instruktioner

1. Installér iperf på en klient og på en server.
2. Iperf fra klient til server.
3. Matcher det din forventning til hastigheden.

## Links

- none supplied