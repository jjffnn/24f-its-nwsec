---
hide:
  - footer
---

# Øvelse 32 easy rsa

## Information

Let's encrypt givere mulighed for at udstede certifikater som bruges eksternt. De fleste organisationer bruger en internt PKI struktur, e.g. fra deres windows domæne controller.

Der er også mulighed for DNS-01 ACME metoden til at udstede certifikater com bruges internt, men det er out-of-scope her.

Man kan sætte en intern PKI op vha. easy-rsa. Dette bruges f.eks. til certikater til VPN forbindelser eller andre cruver certifikater.


## Instruktioner


0. Kend til certifikater, certificate signing request, certificate authority og certificate chains.

  Se e.g. [her](https://dahamposithapathiraja.medium.com/how-https-works-5c44fd3f694c) og [her](https://knowledge.digicert.com/solution/SO16297.html)

1. Installér en easy-rsa pki

  Se [dokumentationen](https://easy-rsa.readthedocs.io/en/latest/#obtaining-and-using-easy-rsa)

  Dette burde ske på en "sikker" host som ikke er webserveren, men til øvelsen kan det være den samme.

2. Configurér en CA

  Se [dokumentationen](https://easy-rsa.readthedocs.io/en/latest/#using-easy-rsa-as-a-ca). Det samme bare fra en [anden kilde](https://wiki.gentoo.org/wiki/Create_a_Public_Key_Infrastructure_Using_the_easy-rsa_Scripts)

2. Opret en ny vm med en webserver

  Brug evt. webserveren fra digital ocean, eller følg [guiden](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-20-04)

3. Opret et certifikat til webserveren

4. Tilføj certifikatet til webserveren

  Det er "bare" at kopiere en fil og pege på den fra konfigurationsfilen. Se f.eks. [her](https://phoenixnap.com/kb/install-ssl-certificate-nginx)

5. check at det virker vha. browser og/eller curl


## Links

* [Easy ra docs](https://easy-rsa.readthedocs.io/en/latest/)
* [Honorable mention](https://smallstep.com/blog/build-a-tiny-ca-with-raspberry-pi-yubikey/) fordi jeg vil have såden et setup
* [easy rsa guide fra openvpn](https://openvpn.net/community-resources/setting-up-your-own-certificate-authority-ca/)