---
hide:
  - footer
---

# Øvelse 36 - ARP

## Information

Vi har tidligere snakket om problemer med ARP og spoofing. Se f.eks. Øvelserne med [ettercap](19_ettercap.md)

Det er også med i MITRE att&ck framework.


## Instruktioner

1. Læs om [ARP spoofing](https://attack.mitre.org/techniques/T1557/002/) og [DHCP spoofing](https://attack.mitre.org/techniques/T1557/003/) som techniques

2. Se på mitigations og giv eksempler på hvordan de foreslåede tiltag kan begrænse spoofing.

3. Se på detections og giv eksempler på hvordan de foreslåede tiltag kan detektere spoofing.



## Links

Se ovenfor.
