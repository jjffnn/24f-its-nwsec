---
hide:
  - footer
---

# Øvelse 6 - more internet speed

## Information

`Iperf` er et værktøj til den slags.

## Instruktioner

1. Installér iperf
2. Kør Iperf mod en public server, e.g. `iperf3 -c <some server> -t 10 -i 1 -p <some port>`
3. Kør Iperf mod en public server (modsat retning), e.g. `iperf3 -c <some server> -t 10 -i 1 -p <some port> -R`
4. Sammenlign resultat. Var hastigheder som forventet?

Note: Der er skrappe begrænsninger på brugen af de forskellige servere, f.eks at der kun er en ad gangen der kan bruge servicen.

## Links

Iperf relaterede links:

* [iperf commands](https://iperf.fr/iperf-servers.php)
* [public iperf server](https://fasterdata.es.net/performance-testing/network-troubleshooting-tools/iperf/)
