---
hide:
  - footer
---

# Øvelse 3 - Fejlfinding

## Information

Dette er en øvelse i fejlfinding, og formålet er at få testet og belyst din tankegang når du arbejder struktureret.

Tag et problem du har i løbet af ugen, og gennemarbejd det.

## Instruktioner

1. Vælg et problem, gerne når det opstår, alternativt på bagkant.
    Det kunne være printerfejl, wifi connection issues, hjemmesider der er ‘væk’, langsomt internet, certifikat fejl, mv
2. Beskriv problemet. Vær opmærksom på forskellen mellem hvad der er observeret, og hvad der er “rygmarvsreaktion”.
3. Beskriv hvad du tror problemet er (ie. formuler en hypotese)
4. Find oplysninger der underbygger/afkræfter ideen (ie. se i logfiler, lav tests, check lysdioder, …) og skriv det ned
5. Hvis ideen blev afkræftet, lav en ny hypotese.
6. Beslut hvad der kan gøres for at afhjælpe eller mitigere fejlen, hvis den skulle komme igen.
7. Implementer det hvis ressourcerne tillader det, eller beskriv hvordan det skal håndteres til næste gang.

**Bonus spørgsmål:** Hvordan håndterede du at nå grænsen for din viden og/eller forståelse af problemet eller et del-element?

## Links

Ingen links, men beskrivelse af 2 meget forskellige måder at fejlfinde på (den ene er god...)

- Root cause analysis
    - Fejlen er tydelig et sted, og så skal man følge kæden af data flow og processer tilbage til hvor den virkelige fejl er.
    Dokumentér undervejs.
- YOLO
    - spredehagl, og når det virker igen så stopper man
    uden at man kan reproducere eller ved hvad der var galt
