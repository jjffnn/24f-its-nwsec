---
hide:
  - footer
---

# Øvelse 29 - Suricata live

## Information

Indtil nu har du brugt Suricata til at analysere pcap filer, ikke fordi det er det bedste værktøj til det, men for at få en forståelse af hvad suricata er og hvordan detekterings regler fungerer. 

I praksis vil du nok bruge regler som er skrevet af andre, måske suppleret med nogle få af dine egne regler.

Der findes åbne community regler som du kan bruge i et IDS/IPS som Suricata eller Snort.  

Emerging Threats er et godt sted at starte når det gælder community regler, de har tusindvis af regler og det kan godt være overvældende at arbejde med dem fra terminalen.  

Derfor skal du i denne øvelse bruge en [opnsense](https://opnsense.org/) router som giver mulighed for at installere suricata som en pakke i routeren.  

Routeren har et web interface der giver et rimeligt overblik over hvad der foregår.

Du skal selvfølgelig dokumentere dit arbejde undervejs.

## Instruktioner

1. Hent opnsense [.ova](https://drive.google.com/file/d/1BIhjv21642vOsmE6RNqa-jX9MtzrySCx/view?usp=sharing) filen
2. Mens du venter, så dan dig et overblik over hvordan [Emerging Threaths](https://rules.emergingthreats.net/open/) reglerne er organiseret og tag også et kig på et par regler, de kan hurtigt blive komplekse!
3. Åbn og konfigurer opnsense maskinen: 
    1. `vmnet8` på `network adapter 1`
    2. `vmnet1` på `network adapter 2`
    3. Start maskinen  
    4. Noter routerens lan og wan adresser.  

    Info om opnsense vm opsætning:
    ```bash
      root:root1234
      wan nwadapter 1: addr from dhcp
      lan nwadapter 2: 192.168.1.0/24 dhcp server enabled
      mngt iface: 192.168.1.1
    ```

4. Sæt en vm (victim) på opnsense lan interfacet `vmnet1`
5. Åbn en browser på victim og log på opnsense management interfacet: `http://192.168.1.1`
5. Sæt en kali maskine (attacker) på opnsense lan interfacet `vmnet1`
6. Tegn et netværksdiagram over dit netværk
7. I opnsense management interfacet skal du navigere til `services->intrusion detection->administration`  
Her skal du vinge af i `enabled`, `Promiscuous mode` og du skal sætte interfaces til både `wan` og `lan`.  
Husk at klikke på `Apply` i bunden af vinduet!  
![opnsense ids administration](../../images/opnsense_ids_administration.png)
8. Gå til `Download` fanen og find regelsættet der hedder `ET open/emerging-scan` og ving det af, klik på `enable selected` og derefter `Download & update rules`  
![opnsense ids download rules](../../images/opnsense_ids_download.png)
9. Gå til `Rules` fanen og marker alle regler (der er 361! så find knappen der vælger dem alle på en gang) og aktiver dem, klik derefter på `Apply`  
![opnsense ids rules](../../images/opnsense_ids_rules.png) 
10. Gå til `Alerts` fanen som gerne skulle være tom, det er her du vil kunne se hvad der registreres af suricata
11. På victim maskinen start en service der lytter på en port, f.eks med `sudo python3 -m http.server`
11. På Kali maskinen lav en nmap scanning på victim maskinens ip adresse f.eks `sudo nmap -v -sV 192.168.1.101` (erstat ip med den adresse som din victim maskine har)
12. Refresh `alerts` og du burde kunne se at der er blevet detekteret noget  
![opnsense ids alerts](../../images/opnsense_ids_alerts.png)  
Hvis der ikke er nogle alerts så gennemgå ovenstående og fejlfind :-)
11. Undersøg dine alerts og genfind reglerne på [Emerging Threaths](https://rules.emergingthreats.net/open/)
12. Lav en nmap scanning der ikke bliver fanget af suricata, altså ikke alerter.  
Forklar hvilken type nmap scanning du lavede 
13. Lav en nmap scanning der laver så mange alerts som muligt (mere end 4)  
Forklar hvilken type nmap scanning du lavede 

**Bonus**

Nu hvor du har et lille ids/ips lab så har du nu mulighed for at lege og lære :-)  
Er du f.eks ikke nysgerrig efter at prøve flere regler ? måske aktivere alle reglerne og afprøve med forskellige Kali værktøjer!

1. Aktiver alle reglerne i opnsense suricata
2. Lav et angreb med [metasploit framework](https://www.kali.org/tools/metasploit-framework/) fra kali maskinen
3. Der er en [youtube video](https://youtu.be/oUp09mcn3Zc) der viser hvordan

Find selv på andre værktøjer du vil prøve og tænk over om du kan bruge det du har lært om intrusion detection og intrusion prevention, i semesterprojektet.

## Links

- [opnsense dokumentation](https://docs.opnsense.org/)
- [opnsense ids/ips docs](https://docs.opnsense.org/manual/ips.html)
- [Video: Setup Suricata IDS/IPS on OPNsense](https://youtu.be/mGzR_9-91Rs)