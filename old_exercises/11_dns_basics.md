---
hide:
  - footer
---

# Øvelse 11 - DNS basics

## Information

Domain Name System - DNS bruger du hver dag, det samme gør sikkerhedsprofesionelle og trusselsaktører.  
Formålet med denne Øvelse er at få indsigt i hvordan DNS grundlæggende fungerer.

## Instruktioner

1. Gennemfør rummet [https://tryhackme.com/room/dnsindetail](https://tryhackme.com/room/dnsindetail)
2. Skriv dine opdagelser ned undervejs og dokumenter det på gitlab

Besvar herefter følgende om DNS og dokumenter undervejs:

1. Forklar med dine egne ord hvad DNS er og hvad det bruges til.
2. Hvilke dele af CIA er aktuelle og hvordan kan DNS sikres? [https://blog.cloudflare.com/dns-encryption-explained/](https://blog.cloudflare.com/dns-encryption-explained/)
3. Hvilken port benytter DNS ?
4. Beskriv DNS domæne hierakiet
5. Forklar disse DNS records: A, AAAA, MX, TXT og CNAME.
6. Brug `nslookup` til at undersøge hvor mange mailservere ucl.dk har
7. Brug `dig` til at finde txt records for ucl.dk 
2. Brug wireshark til:
    1. Filtrere DNS trafik. Dokumenter hvilket filter du skal bruge for kun at se DNS trafik
    2. Lav screenshots af eksempler på DNS trafik 
 
## Links

- Moozer DNS oversigt [https://moozer.gitlab.io/course-networking-basisc/06_domain_name_system/](https://moozer.gitlab.io/course-networking-basisc/06_domain_name_system/)
