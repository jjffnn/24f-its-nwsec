---
hide:
  - footer
---

# Øvelse 24 - udp amplification attack

## Information

UDP amplification attack er en måde at lave DoS (denial of service) angreb.

## Instruktioner

1. Forklar kort hvordan UDP virker og vhorfor IP spoofing kan virke med UDP. 
  * Lav et eksempel
2. Forklar kort hvad et denial of service angreb er
3. Lav et eksempel med DNS eller NTP der viser et DoS angreb
  * Hvilken "service" bliver denied?
  * Alle systemer har bottlenecks, kom med relevante eksempler i denne kontekst
