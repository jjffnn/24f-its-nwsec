---
hide:
  - footer
---

# Øvelse 17 - logs

## Information

Formålet med Øvelsen er at få et indtryk af log og hvad man kan læse af dem.

Specifikt vil vi connecte via ssh og genfinde dataene i loggen.

## Instruktioner

1. Start default open bsd router og kali linux
2. Log ind i konsollen på routeren og notér ip adressen på det eksterne interface: `ifconfig em0`
2. Åben en terminal i kali, og forbind routern: `ssh sysuser@<router ip>`
3. Se hvad der er af logs: `cd /var/log` efterfult af `ls`
4. Hvad er der af log filer?
    * `messages` er vigtig i bsd (på linux er den tilsvarende `syslog`)
    * `auth` indeholder authentication events
    * Der er ikke DNS logs tilgængelig i default konfigurationen, men det kan slås til. Se [logfile directive](https://linux.die.net/man/5/unbound.conf)

4. Tricks: `tail -f authlog` og start en ny ssh connection til routeren. Hvad sker der?
5. Log filer bliver allerede hentet ind fra grafana serveren til grafana serveren. Start `server-mon02`.
6. Gå til grafana web interface. Default url er `http://192.168.111.20:3000`
7. Gå til explore og lav en simpel loki søgning, f.eks. `{filename="/var/log/syslog"} |= "ssh"`
8. Lav også: `{filename="/var/log/auth.log"} |= "ssh"`
9. Sammelign



## Links

- Log filer og lidt om hvordan de bruges: [https://www.cyberciti.biz/faq/linux-log-files-location-and-how-do-i-view-logs-files](https://www.cyberciti.biz/faq/linux-log-files-location-and-how-do-i-view-logs-files)
- [LogQL, query language for loki](https://grafana.com/docs/loki/latest/logql/log_queries/). Dette er et selvstændigt query language.
