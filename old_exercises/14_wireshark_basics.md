---
hide:
  - footer
---

# Øvelse 14 - Wireshark basic

## Information

Vi vil connecte til en HTTP server og se på trafikken

Formålet med Øvelsen er at forstå netværkstrafik og se på HTTP

## Instruktioner

1. Start wireshark
2. Gå ind på http [http://mirror.one.com/debian/](http://mirror.one.com/debian/) og brug din browsers `view source`
3. Genfind trafikken i wireshark, brug follow tcp stream
4. Hent den samme side via https [https://mirror.one.com/debian/](https://mirror.one.com/debian/)
5. Genfind trafikken i wireshark, brug follow tcp stream
6. Brug curl/wget fra kommando linien til at hente HTTP fra linket i punkt 3
7. Sammenlign med resultatet fra browserens `view source`

## Links

- Lidt om [HTTP fra cloudflare](https://www.cloudflare.com/en-gb/learning/ddos/glossary/hypertext-transfer-protocol-http)
