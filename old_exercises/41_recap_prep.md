---
hide:
  - footer
---

# Øvelse 41 - Repetitions forberedelse

## Information

Emnerne til eksamen er nu frigivet og til næste undervisning skal i læse emnerne og forholde jer til dem.  
Der vil nok være ting i er i tvivl om i emnerne og derfor er det vigtigt at i kan stille spørgsmål når vi holder repetition næste mandag.  

Vi anbefaler kraftigt at i gennemgår emnerne sammen, så i kan bruge hinanden til at finde ud af hvad i mangler at få afklaret.

Målet er at i er klædt så godt som muligt på til at forberede jer til eksamen.  

[Link til eksamensopgave](../../other_docs/eksamens_opgave.md)

## Instruktioner

1. Gennemgå eksamensØvelsen sammen med andre fra klassen, det kan være i jeres team eller på tværs af teams.
2. Lav en liste med spørgsmål til emnerne, vi forventer at i har mindst et spørgsmål pr. studerende!

## Links

