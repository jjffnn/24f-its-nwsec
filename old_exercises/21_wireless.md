---
hide:
  - footer
---

# Øvelse 21 - Wireless 

## Information

Der er mange interessante angreb på wireless systemer. 

Aircrack er godt sted at starte. Generelt ender det i et hardware problem, da man skal have en maskine med et wireless kort, og gerne en anden internet adgang ved samme lejlighed.

En god løsning er at have et usb wireless netværkskort som kan mappes direkte ind i f.eks. Kali.


## Instruktioner

Følg [denne guide](https://nooblinux.com/crack-wpa-wpa2-wifi-passwords-using-aircrack-ng-kali-linux/)