---
hide:
  - footer
---

# Øvelse 38 - Angrebsflade (attack surface)

## Information

I denne Øvelse skal i undersøge hvad attack surface og attack vector er.  
I skal også undersøge hvordan det kan bruges af både aktører og trusselsaktører.

Der er givet nogle links til at komme i gang med at researche, find gerne selv flere links men husk at være kritiske ifht. kilden.
Det er vigtigt at i snakker om spørgsmålene så i opnår en fælles velreflekteret forståelse og viden.

## Instruktioner

Besvar følgende:

1. Forklar hvad attack surface er
2. Forklar hvad attack vector er
3. Hvad siger CIS18 ?
4. Hvad siger MITRE ? (husk sub techniques)


## Links

- okta attack surface [https://www.okta.com/identity-101/what-is-an-attack-surface/](https://www.okta.com/identity-101/what-is-an-attack-surface/)
- fortinet attack surface[https://www.fortinet.com/resources/cyberglossary/attack-surface](https://www.fortinet.com/resources/cyberglossary/attack-surface)
- [Crowdstrike](https://www.crowdstrike.com/cybersecurity-101/attack-surface/)
- MITRE Active Scanning [https://attack.mitre.org/techniques/T1595/](https://attack.mitre.org/techniques/T1595/)
- CIS18 Control 7 [https://controls-assessment-specification.readthedocs.io/en/stable/control-7/index.html](https://controls-assessment-specification.readthedocs.io/en/stable/control-7/index.html)
