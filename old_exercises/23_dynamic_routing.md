---
hide:
  - footer
---

# Øvelse 23 - Dynamic routing

## Information

"Routing" er den mekanisme på lag 3 som håndterer at sende pakker på tværs af subnets.

Dynamic routing er når man opdaterer routing tabellen løbende med info udefra. Vi vil ikke dykke ned i detaljerne.

* OSPF er en protokol til internt brug
* BGP er protokollen der bruges på internettet


## Instruktioner

1. Forklar hurtigt hvorfor dynamisk routing er essentiel for internettet
2. Lav et netværkseksempel der illustrerer hvorfor vi ønsker at opdatere routing tabeller
  * inkludér hvadder er i de enkelte enheders routing tabeler
3. Bonus: Hvordan passer dette med CIA?
