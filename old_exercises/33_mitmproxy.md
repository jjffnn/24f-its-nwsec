---
hide:
  - footer
---

# Øvelse 33 - Mitm proxy

## Information

Mitm proxy er en proxy der kan lave https decryption ved at lave et man-in-the-middle angreb, og generere certifikater on-the-fly til at matche det domæne der besøges.

Læs mere [her](https://mitmproxy.org/)

Mitmproxy installerer ikke pt, så at teste live gør vi ikke.

## Instruktioner

1. Læs op på SSL inspection
2. Forklar hvad er er og hvad det bruges til
5. Tegn et netværksdiagram med relevante enheder og forklar hvad der sker.


## Links

* Https inspection fra cloudflare: https://www.cloudflare.com/en-gb/learning/security/what-is-https-inspection/
