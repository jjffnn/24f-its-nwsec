---
hide:
  - footer
---

# Øvelse 34 - MITRE att&ck framework

## Information

[MITRE att&ck framework](https://attack.mitre.org/) er en "standard" for at beskrive angreb og metoder.

De har mange resource tilgængelige.

* [Getting started](https://attack.mitre.org/resources/)
* [Deres ebook](https://www.mitre.org/sites/default/files/2021-11/getting-started-with-attack-october-2019.pdf)

## Instruktioner

1. Lav en hurtig beskrivelse af hvad MITRE att&ck framework er. Inkludér de tre hovedkategorier: Tactics, Techniques and Procedures

    Ja, chatgpt kan give et svar meget hurtigt, men det er ikke pointen og det ved I godt.

2. Bonus: Hvorfor TTPs? Hvor kommer det fra?

3. Find 2-3 eksempler i hver kategori. Supplér med en sætning eller to om hvad de dækker over.

    Eksemplerne skal være forståelige og/eller cool.


## Links

* se ovenfor
