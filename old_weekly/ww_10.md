---
Week: 10
Content: XXX
Material: xxx
Initials: XXXX
# hide:
#  - footer
---

# Uge 10 - Netværks protokoller **(KLADDE)**

## Emner

Dagens emne er netværkstrafik og hvordan man analyserer det. Data skal samles op og vi ser på lidt tools til at arbejde med det.

Det forventes at den studerende har en grundlæggende forståelse for forskellige netværksprotokoller på forhånd, såsom DNS og HTTP.

- Online companion til dagen er [her](https://moozer.gitlab.io/course-networking-security/04_packets/)

## Ugens emner er

- Netværkstrafik
- Analyse af netværkstrafik

## Mål for ugen

Herunder kan du læse ugens forskellige mål

### Praktiske mål

- [Øvelse 9 - Protokoller](https://ucl-pba-its.gitlab.io/23f-its-nwsec/exercises/09_protokoller/)
- [Øvelse 10 - Trafikanalyse](https://ucl-pba-its.gitlab.io/23f-its-nwsec/exercises/10_trafikanalyse/)
- [SSL dump Øvelse](https://moozer.gitlab.io/course-networking-security/04_packets/tls_protocol/)

### Læringsmål der arbejdes med i faget denne uge

#### Overordnede læringsmål fra studie ordningen

- **_Den studerende har viden om og forståelse for_**  
    - Dybdegående kendskab til flere af de mest anvendte internet protokoller (ssl)
- **_Den studerende kan_**
    - Teste netværk for angreb rettet mod de mest anvendte protokoller
- **_Den studerende kan håndtere udviklingsorienterede situationer herunder_**
    - Monitorere og administrere et netværks komponenter

#### Læringsmål den studerende kan bruge til selvvurdering

- Den studerende kan forklare TLS
- Den studerende kan bruge relevante analyse værktøjer til at undersøge netværkstrafik.

## Afleveringer

Øvelser og refleksioner over læring dokumenteret på studiegruppernes gitlab pages:  

1. [byteshield](https://24f-its-semesterprojekt-studerende.gitlab.io/1-byteshield/)
2. [cryptohex](https://24f-its-semesterprojekt-studerende.gitlab.io/2-cryptohex/)
3. [hexforce](https://24f-its-semesterprojekt-studerende.gitlab.io/3-hexforce/)
4. [secminds](https://24f-its-semesterprojekt-studerende.gitlab.io/4-secminds/)
5. [hexdef](https://24f-its-semesterprojekt-studerende.gitlab.io/5-hexdef/)
6. [ciphercrew](https://24f-its-semesterprojekt-studerende.gitlab.io/6-ciphercrew/)
7. [codecrypts](https://24f-its-semesterprojekt-studerende.gitlab.io/7-codecrypts/)

## Skema

### Onsdag

|  Tid  | Aktivitet              |
| :---: | :--------------------- |
| 8:15  | Introduktion til dagen |
| 8:30  | Feedback på jeres gitlab dokumentation |
| 9:00  | Øvelser K1/K4          |
| 11:30 | Frokost                |
| 12:15 | Øvelser K2/K3          |
| 15:30 | Fyraften               |

## Kommentarer

- 4 lektioner med underviser, resten af dagen Gruppearbejde, øvelser og selvstudie

## Links

- None supplied
